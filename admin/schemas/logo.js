export default {
  name: 'logo',
  title: 'Logotypes',
  type: 'document',
  fields: [
    {
      name: 'image',
      title: 'Logo',
      type: 'array',
      of: [{type: 'image'}],
      options: {
        hotspot: true,
      },
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'Logotypes for custom page',
      }
    },
  },
}
