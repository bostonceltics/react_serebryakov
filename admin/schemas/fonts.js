import {RiFontSize} from 'react-icons/ri'
import tarifs from '../../data/tarifs.json'
import licenses from '../../data/licenses.json'

const variations = (family) => {
  let variationArr = []
  tarifs.map((tarif) => {
    variationArr.push({
      type: 'number',
      name: `${family}${tarif.fieldset}${tarif.name}`,
      title: tarif.title,
      fieldset: tarif.fieldset,
      validation: (rule) => rule.required(),
    })
  })
  return variationArr
}
const basicArray = variations('')
const familyArray = variations('family_')

let fieldsetsArray = []

licenses.map((license) => {
  fieldsetsArray.push({
    name: license.name,
    title: license.title,
    options: {
      collapsible: true,
      collapsed: true,
      columns: 2,
    },
  })
})

export default {
  name: 'fonts',
  title: 'Fonts',
  type: 'document',
  icon: RiFontSize,
  groups: [
    {
      name: 'email_zip',
      title: 'E-mail files',
    },
  ],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: (rule) => rule.required(),
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 90,
      },
      validation: (rule) => rule.required(),
    },
    {
      name: 'preview',
      title: 'Font for preview',
      type: 'file',
      validation: (rule) => rule.required(),
    },
    {
      name: 'fonts',
      title: 'Fonts',
      type: 'array',
      of: [
        {
          type: 'object',
          name: 'font_item',
          fields: [
            {
              type: 'string',
              name: 'name',
              title: 'Name',
              validation: (rule) => rule.required(),
            },
            {
              type: 'string',
              name: 'style',
              title: 'Style',
              validation: (rule) => rule.required().lowercase(),
            },
            {type: 'file', name: 'font', title: 'Font', validation: (rule) => rule.required()},
            {type: 'string', name: 'word', title: 'Word'},
            {
              type: 'object',
              name: 'prices',
              description: `Type price in USD`,
              options: {
                collapsible: true,
                collapsed: true,
              },
              fieldsets: fieldsetsArray,
              fields: basicArray,
            },
            {
              type: 'object',
              name: 'email',
              options: {
                collapsible: true,
                collapsed: true,
              },
              fields: [
                {
                  type: 'file',
                  name: 'email_font_web',
                  title: 'WEB font',
                  description: `FILE NAME FORMAT: serebryakovtf-fontname-style.web.zip`,
                  validation: (rule) => rule.required(),
                },
                {
                  type: 'file',
                  name: 'email_font_otf',
                  title: 'OTF font',
                  description: `FILE NAME FORMAT: serebryakovtf-fontname-style.otf.zip)`,
                  validation: (rule) => rule.required(),
                },
              ],
            },
          ],
        },
      ],
    },
    {
      name: 'is_family',
      title: 'Has family?',
      type: 'boolean',
    },
    {
      name: 'family_prices',
      title: 'Family prices',
      description: `Type price in USD`,
      type: 'object',
      hidden: ({document}) => !document.is_family,
      options: {
        collapsible: true,
        collapsed: true,
      },
      fieldsets: fieldsetsArray,
      fields: familyArray,
    },
    {
      name: 'family_email',
      title: 'Family email',
      type: 'object',
      hidden: ({document}) => !document.is_family,
      options: {
        collapsible: true,
        collapsed: true,
      },
      fields: [
        {
          type: 'file',
          name: 'email_font_web',
          title: 'WEB font',
          description: `FILE NAME FORMAT: serebryakovtf-fontname-style.web.zip`,
          validation: (rule) => rule.required(),
        },
        {
          type: 'file',
          name: 'email_font_otf',
          title: 'OTF font',
          description: `FILE NAME FORMAT: serebryakovtf-fontname-style.otf.zip`,
          validation: (rule) => rule.required(),
        },
      ],
    },
    {
      name: 'italic',
      title: 'Has Italic?',
      type: 'boolean',
    },
    {
      name: 'lowercase',
      title: 'Has Lowercase?',
      type: 'boolean',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare({title}) {
      return {title}
    },
  },
}
