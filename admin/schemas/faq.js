import {AiOutlineQuestion} from 'react-icons/ai'

export default {
  name: 'faq',
  title: 'FAQ',
  type: 'document',
  icon: AiOutlineQuestion,
  groups: [
    {
      name: 'en',
      title: 'Eng',
      default: true,
    },
    {
      name: 'ru',
      title: 'Rus',
    },
  ],
  fields: [
    {
      name: 'question_en',
      title: 'Question (En)',
      type: 'text',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'answer_en',
      title: 'Answer (En)',
      type: 'text',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'question_ru',
      title: 'Question (Ru)',
      type: 'text',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'answer_ru',
      title: 'Answer (Ru)',
      type: 'text',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
  ],
  preview: {
    select: {
      title: 'question_en',
    },
    prepare({title}) {
      return {title}
    },
  },
}
