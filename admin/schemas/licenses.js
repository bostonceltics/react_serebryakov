import {AiOutlineFileProtect} from 'react-icons/ai'

export default {
  name: 'licenses',
  title: 'Licenses',
  type: 'document',
  icon: AiOutlineFileProtect,
  groups: [
    {
      name: 'en',
      title: 'Eng',
      default: true,
    },
    {
      name: 'ru',
      title: 'Rus',
    },
  ],
  fields: [
    {
      name: 'title_en',
      title: 'Title (En)',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'who_en',
      title: 'Who can use it (En)',
      type: 'text',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'desc_en',
      title: 'Description (En)',
      type: 'text',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'file_en',
      title: 'License file (En)',
      type: 'file',
      group: 'en',
    },
    {
      name: 'hint_en',
      title: 'Hint text (En)',
      type: 'text',
      group: 'en',
    },
    {
      name: 'title_ru',
      title: 'Title (Ru)',
      type: 'string',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'who_ru',
      title: 'Who can use it (Ru)',
      type: 'text',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'desc_ru',
      title: 'Description (Ru)',
      type: 'text',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'file_ru',
      title: 'License file (Ru)',
      type: 'file',
      group: 'ru',
    },
    {
      name: 'hint_ru',
      title: 'Hint text (Ru)',
      type: 'text',
      group: 'ru',
    },
  ],
  preview: {
    select: {
      title: 'title_en',
    },
    prepare({title}) {
      return {title}
    },
  },
}
