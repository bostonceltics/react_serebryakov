import {TbShoppingCartDiscount} from 'react-icons/tb'

export default {
  name: 'coupons',
  title: 'Coupons',
  type: 'document',
  icon: TbShoppingCartDiscount,
  fields: [
    {
      name: 'coupon',
      title: 'Coupon',
      type: 'string',
      validation: (rule) => rule.required(),
    },
    {
      name: 'type',
      title: 'Discount type',
      type: 'string',
      options: {
        list: [
          {title: 'Percentage discount', value: 'percentage_discount'},
          {title: 'Fixed cart discount', value: 'fixed_cart_discount'},
        ],
      },
      validation: (rule) => rule.required(),
    },
    {
      name: 'amount',
      title: 'Coupon amount',
      type: 'number',
      validation: (rule) => rule.required(),
    },
    {
      title: 'Coupon expiry date',
      name: 'expiry_date',
      type: 'date',
      options: {
        dateFormat: 'YYYY-MM-DD',
        calendarTodayLabel: 'Today',
      },
    },
    {
      name: 'is_unlimited',
      title: 'Unlimited usage',
      type: 'boolean',
      initialValue: true,
    },
    {
      name: 'limit',
      title: 'Usage limit per coupon',
      type: 'number',
      validation: (rule) => rule.required(),
      hidden: ({document}) => document.is_unlimited,
    },
  ],
  preview: {
    select: {
      title: 'coupon',
    },
    prepare({title}) {
      return {title}
    },
  },
}
