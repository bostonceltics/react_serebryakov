export default {
  name: 'info',
  title: 'Info',
  type: 'document',
  groups: [
    {
      name: 'en',
      title: 'Eng',
      default: true,
    },
    {
      name: 'ru',
      title: 'Rus',
    },
  ],
  fields: [
    {
      name: 'title_en',
      title: 'Title (En)',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      group: 'en',
      options: {
        source: 'title_en',
        maxLength: 90,
      },
      validation: (rule) => rule.required(),
    },
    {
      name: 'title_ru',
      title: 'Title (Ru)',
      type: 'string',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'content_en',
      type: 'array',
      title: 'Content (En)',
      group: 'en',
      of: [
        {
          type: 'block',
        },
        {
          type: 'image',
        },
        {
          type: 'code',
        },
      ],
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'Info page',
      }
    },
  },
}
