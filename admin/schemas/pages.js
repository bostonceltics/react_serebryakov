import {AiOutlineFileText} from 'react-icons/ai'

export default {
  name: 'pages',
  title: 'Pages',
  type: 'document',
  icon: AiOutlineFileText,
  groups: [
    {
      name: 'en',
      title: 'Eng',
      default: true,
    },
    {
      name: 'ru',
      title: 'Rus',
    },
  ],
  fields: [
    {
      name: 'title_en',
      title: 'Title (En)',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      group: 'en',
      options: {
        source: 'title_en',
        maxLength: 90,
      },
      validation: (rule) => rule.required(),
    },
    {
      name: 'content_en',
      title: 'Content (En)',
      type: 'array',
      group: 'en',
      validation: (rule) => rule.required(),
      of: [
        {
          type: 'block',
          marks: {
            decorators: [
              {title: 'Strong', value: 'strong'},
              {title: 'Emphasis', value: 'em'},
              {title: 'Code', value: 'code'},
              {title: 'Highlight', value: 'highlight'},
            ],
          },
        },
      ],
    },
    {
      name: 'title_ru',
      title: 'Title (Ru)',
      type: 'string',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'content_ru',
      title: 'Content (Ru)',
      type: 'array',
      group: 'ru',
      validation: (rule) => rule.required(),
      of: [
        {
          type: 'block',
          marks: {
            decorators: [
              {title: 'Strong', value: 'strong'},
              {title: 'Emphasis', value: 'em'},
              {title: 'Code', value: 'code'},
              {title: 'Highlight', value: 'highlight'},
            ],
          },
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'title_en',
    },
    prepare({title}) {
      return {title}
    },
  },
}
