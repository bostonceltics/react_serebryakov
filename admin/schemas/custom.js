import {AiOutlineFormatPainter} from 'react-icons/ai'

export default {
  name: 'Custom',
  title: 'Custom',
  type: 'document',
  icon: AiOutlineFormatPainter,
  groups: [
    {
      name: 'en',
      title: 'Eng',
    },
    {
      name: 'ru',
      title: 'Rus',
    },
  ],
  fields: [
    {
      name: 'title_en',
      title: 'Title (En)',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'desc_en',
      title: 'Description (En)',
      type: 'string',
      group: 'en',
      validation: (rule) => rule.required(),
    },
    {
      name: 'title_ru',
      title: 'Title (Ru)',
      type: 'string',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'desc_ru',
      title: 'Description (Ru)',
      type: 'string',
      group: 'ru',
      validation: (rule) => rule.required(),
    },
    {
      name: 'gallery',
      title: 'Gallery',
      type: 'array',
      of: [{type: 'image'}],
      options: {
        hotspot: true,
      },
      validation: (rule) => rule.required(),
    },
  ],
  preview: {
    select: {
      title: 'title_en',
    },
    prepare({title}) {
      return {title}
    },
  },
}
