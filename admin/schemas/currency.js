export default {
  name: 'currency',
  title: 'Currency',
  type: 'document',
  fields: [
    {
      name: 'byn',
      title: 'BYN',
      type: 'boolean',
    },
    {
      name: 'rub',
      title: 'RUB',
      type: 'boolean',
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'These currincies will display on the site.',
      }
    },
  },
}
