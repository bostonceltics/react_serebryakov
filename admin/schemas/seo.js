export default {
  name: 'seo',
  title: 'Seo',
  type: 'document',
  fields: [
    {
      name: 'title',
      title: 'Site Title',
      type: 'string',
    },
    {
      name: 'description',
      title: 'Site Description',
      type: 'text',
    },
    {
      name: 'ga_code',
      title: 'Google Analytics code',
      type: 'text',
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'SEO settings',
      }
    },
  },
}
