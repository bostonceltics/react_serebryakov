export default {
  name: 'navigation',
  title: 'Navigation',
  type: 'document',
  fields: [
    {
      name: 'header_menu',
      title: 'Header menu',
      type: 'array',
      of: [
        {
          type: 'object',
          name: 'header_menu_item',
          fields: [
            {type: 'string', name: 'text', title: 'Text'},
            {
              type: 'string',
              name: 'slug',
              title: 'Slug',
            },
          ],
        },
      ],
    },
    {
      name: 'footer_menu',
      title: 'Footer menu',
      type: 'array',
      of: [
        {
          type: 'object',
          name: 'footer_menu_item',
          fields: [
            {type: 'string', name: 'text', title: 'Text'},
            {
              type: 'string',
              name: 'slug',
              title: 'Slug',
            },
            {type: 'boolean', name: 'target', title: 'Open in new window'},
          ],
        },
      ],
    },
  ],
  preview: {
    prepare() {
      return {
        title: 'Menu settings',
      }
    },
  },
}
