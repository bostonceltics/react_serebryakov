import faq from './faq'
import licenses from './licenses'
import custom from './custom'
import seo from './seo'
import logo from './logo'
import currency from './currency'
import navigation from './navigation'
import fonts from './fonts'
import info from './info'
import pages from './pages'
import coupons from './coupons'

export const schemaTypes = [
  fonts,
  faq,
  licenses,
  custom,
  seo,
  currency,
  navigation,
  info,
  pages,
  logo,
  coupons,
]
