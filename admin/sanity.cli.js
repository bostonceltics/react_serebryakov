import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'vfp5mycn',
    dataset: 'production',
  },
})
