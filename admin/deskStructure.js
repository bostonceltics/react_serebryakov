import {AiOutlineSetting} from 'react-icons/ai'
import {IoPulseOutline} from 'react-icons/io5'
import {IoIosInformationCircleOutline} from 'react-icons/io'
import {GrNavigate} from 'react-icons/gr'
import {BsCurrencyExchange} from 'react-icons/bs'
import {TbIcons} from 'react-icons/tb'

export const myStructure = (S) =>
  S.list()
    .title('Content')
    .items([
      S.listItem()
        .title('Settings')
        .icon(AiOutlineSetting)
        .child(
          S.list()
            // Sets a title for our new list
            .title('Settings Documents')
            // Add items to the array
            // Each will pull one of our new singletons
            .items([
              S.listItem()
                .title('SEO')
                .icon(IoPulseOutline)
                .child(S.document().schemaType('seo').documentId('seo')),
              S.listItem()
                .title('Currency')
                .icon(BsCurrencyExchange)
                .child(S.document().schemaType('currency').documentId('currency')),
              S.listItem()
                .title('Navigation')
                .icon(GrNavigate)
                .child(S.document().schemaType('navigation').documentId('navigation')),
              S.listItem()
                .title('Logotypes')
                .icon(TbIcons)
                .child(S.document().schemaType('logo').documentId('logo')),
            ])
        ),
      S.listItem()
        .title('Info')
        .icon(IoIosInformationCircleOutline)
        .child(S.document().schemaType('info').documentId('info')),
      // We also need to remove the new singletons from the main list
      ...S.documentTypeListItems().filter(
        (listItem) => !['seo', 'currency', 'navigation', 'logo', 'info'].includes(listItem.getId())
      ),
    ])
