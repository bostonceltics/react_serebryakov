import React, { createContext, useContext, useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";
import en from "../data/locales/en";
import ru from "../data/locales/ru";

const Context = createContext();

export const StateContext = ({ children }) => {
  const [fontSlug, setFontSlug] = useState("");
  const [totalPrice, setTotalPrice] = useState(0);
  const [cartItems, setCartItems] = useState([]);
  const [cartItemsEmail, setCartItemsEmail] = useState([]);
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const [siteCurrency, setSiteCurrency] = useState("USD");
  const [siteLang, setSiteLang] = useState("en");

  const isMobile = useMediaQuery({ query: `(max-width: 992px)` });

  const [discount, setDiscount] = useState(0);
  const [discountTotalPrice, setDiscountTotalPrice] = useState(totalPrice);
  const [couponText, setCouponText] = useState("");

  const [checkoutFormData, setCheckoutFormData] = useState("");

  // generate order ID start

  const [orderId, setOrderId] = useState("");
  const orderIdNew = Date.now().toString();

  // generate order ID end

  // save data to localStorage start

  useEffect(() => {
    if (orderId === "" && !window.localStorage.getItem("orderId")) {
      setOrderId(orderIdNew);
    }
    const localOrderId = window.localStorage.getItem("orderId");
    if (localOrderId !== null) {
      setOrderId(JSON.parse(localOrderId));
    }
    const localCartItems = window.localStorage.getItem("cartItems");
    if (localCartItems !== null) {
      setCartItems(JSON.parse(localCartItems));
    }
    const localCartItemsEmail = window.localStorage.getItem("cartItemsEmail");
    if (localCartItemsEmail !== null) {
      setCartItemsEmail(JSON.parse(localCartItemsEmail));
    }
    const localTotalPrice = window.localStorage.getItem("totalPrice");
    if (localTotalPrice !== null) {
      setTotalPrice(JSON.parse(localTotalPrice));
    }
    const localSiteCurrency = window.localStorage.getItem("siteCurrency");
    if (localSiteCurrency !== null) {
      setSiteCurrency(JSON.parse(localSiteCurrency));
    }
    const localSiteLang = window.localStorage.getItem("siteLang");
    if (localSiteLang !== null) {
      setSiteLang(JSON.parse(localSiteLang));
    }
    const localCouponText = window.localStorage.getItem("couponText");
    if (localCouponText !== null) {
      setCouponText(JSON.parse(localCouponText));
    }
    const localDiscount = window.localStorage.getItem("discount");
    if (localDiscount !== null) {
      setDiscount(JSON.parse(localDiscount));
    }
    const localDiscountTotalPrice =
      window.localStorage.getItem("discountTotalPrice");
    if (localDiscountTotalPrice !== null) {
      setDiscountTotalPrice(JSON.parse(localDiscountTotalPrice));
    }
    const checkoutFormDataLocal =
      window.localStorage.getItem("checkoutFormData");
    if (checkoutFormDataLocal !== null) {
      setCheckoutFormData(JSON.parse(checkoutFormDataLocal));
    }
  }, []);

  useEffect(() => {
    window.localStorage.setItem("orderId", JSON.stringify(orderId));
    window.localStorage.setItem("cartItems", JSON.stringify(cartItems));
    window.localStorage.setItem(
      "cartItemsEmail",
      JSON.stringify(cartItemsEmail)
    );
    window.localStorage.setItem("totalPrice", JSON.stringify(totalPrice));
    window.localStorage.setItem("siteCurrency", JSON.stringify(siteCurrency));
    window.localStorage.setItem("siteLang", JSON.stringify(siteLang));
    window.localStorage.setItem("couponText", JSON.stringify(couponText));
    window.localStorage.setItem("discount", JSON.stringify(discount));
    window.localStorage.setItem(
      "discountTotalPrice",
      JSON.stringify(discountTotalPrice)
    );
    window.localStorage.setItem(
      "checkoutFormData",
      JSON.stringify(checkoutFormData)
    );
  }, [
    cartItems,
    cartItemsEmail,
    totalPrice,
    siteCurrency,
    siteLang,
    couponText,
    discount,
    discountTotalPrice,
    orderId,
    checkoutFormData,
  ]);

  // save data to localStorage end

  const onAdd = (product) => {
    const cart = totalPrice + product.price;
    setTotalPrice(cart);
    if (couponText) {
      checkCoupons(couponText, couponsArr, product.price);
    }
    setCartItems([...cartItems, { ...product }]);
  };

  const onAddEmail = () => {
    const data = [];

    document.querySelectorAll(".variation-cart").forEach((el) => {
      const fontNameParent = el.closest(".cart-item");
      const font_name = fontNameParent.querySelector(".font_name").innerText;
      const font_license = el.querySelector(".font_license").innerText;
      const font_tarif =
        el.querySelector("#select").options[
          el.querySelector("#select").selectedIndex
        ].text;
      const font_price = el.querySelector(".font_price").innerText;
      const input_value = el.querySelector("input")?.value;

      data.push({
        font_name,
        font_license,
        font_tarif,
        input_value,
        font_price,
      });
    });

    setCartItemsEmail("");
    setCartItemsEmail(data);
  };

  const searchProduct = (searchProduct) => {
    const result = cartItems.find(
      (item) =>
        item.variation == searchProduct.variation &&
        item.style == searchProduct.style &&
        item.name == searchProduct.name
    );
    return result;
  };

  const checkCartItems = (product) => {
    const searchObject = searchProduct(product);

    const result = { class: "btn-disabled", text: t.add_to_cart_btn_disabled };
    return searchObject && result;
  };

  const onRemove = (product) => {
    const searchObject = searchProduct(product);
    if (searchObject) {
      const newCartItems = cartItems.filter(
        (item) =>
          `${item.variation}${item.style}${item.name}` !==
          `${product.variation}${product.style}${product.name}`
      );
      setTotalPrice((prevTotalPrice) => prevTotalPrice - product.price);
      if (couponText) {
        checkCoupons(couponText, couponsArr, -product.price);
      }

      setCartItems(newCartItems);
    }
  };

  // const removeDublicates = (cartItems) => {
  //   const newCartItems = cartItems.filter(
  //     (value, index, self) =>
  //       index ===
  //       self.findIndex(
  //         (t) =>
  //           t.variation === value.variation &&
  //           t.name === value.name &&
  //           t.style === value.style
  //       )
  //   );
  //   setCartItems(newCartItems);
  // };

  const onUpdate = (productOld, newVariation, newPrice) => {
    const oldPrice = productOld.price;

    const index = cartItems.findIndex(
      (item) =>
        item.variation == productOld.variation &&
        item.style == productOld.style &&
        item.name == productOld.name
    );

    const newCartItems = [];
    cartItems.map((item, i) => {
      if (i === index) {
        item["variation"] = newVariation;
        item["price"] = newPrice;
      }
      newCartItems.push(item);
    });

    setTotalPrice((prevTotalPrice) => prevTotalPrice - oldPrice + newPrice);
    if (couponText) {
      checkCoupons(couponText, couponsArr, -oldPrice + newPrice);
    }

    setCartItems(newCartItems);
  };

  const onUpdateInput = (productOld, inputValue) => {
    const index = cartItems.findIndex(
      (item) =>
        item.variation == productOld.variation &&
        item.style == productOld.style &&
        item.family == productOld.family &&
        item.name == productOld.name
    );

    const newCartItems = [];
    cartItems.map((item, i) => {
      if (i === index) {
        item["inputField"] = inputValue;
      }
      newCartItems.push(item);
    });

    setCartItems(newCartItems);
  };

  const getMinPriceFont = (fonts) => {
    let minPrice = 0;
    if (fonts) {
      fonts.map((font) => {
        let arr = Object.values(font.prices);
        let min = Math.min(...arr);
        if (minPrice > min || minPrice === 0) {
          minPrice = min;
        }
      });
    }
    return minPrice;
  };

  const getMinPriceStyle = (prices) => {
    let minPrice = 0;
    if (prices) {
      const values = Object.values(prices);
      minPrice = Math.min(...values);
    }
    return minPrice;
  };

  const getAmountStyles = (fonts) => {
    return fonts ? fonts.length : 0;
  };

  const getPreviewLink = (preview) => {
    let link = `https://cdn.sanity.io/files/${
      process.env.NEXT_PUBLIC_PROJECT_ID
    }/${process.env.NEXT_PUBLIC_PROJECT_DATASET}/${preview.split("-")[1]}.${
      preview.split("-")[2]
    }`;
    return link;
  };

  // checkCoupons start

  const [couponsArr, setCouponsArr] = useState([]);

  let today = new Date().toISOString().slice(0, 10);
  today = new Date(today);

  const checkCoupons = (value, coupons, product) => {
    const couponField = document.querySelector("#promoCodeInput");
    setCouponsArr(couponsArr);
    setDiscount(0);
    let total = totalPrice;
    setCouponText(value);
    if (product) {
      total = totalPrice + product;
    }
    if (value.length !== 0) {
      if (couponField) {
        couponField.style.width = value.length + "ch";
      }

      const checkCoupon = coupons.find((item) => item.coupon == value);
      if (checkCoupon) {
        const expiredDate = new Date(checkCoupon.expiry_date);
        if (today.getTime() <= expiredDate.getTime()) {
          if (checkCoupon.is_unlimited === true || checkCoupon.limit > 0) {
            if (checkCoupon.type === "percentage_discount") {
              setDiscountTotalPrice(
                total - Math.round((total * checkCoupon.amount) / 100)
              );
              setDiscount(Math.round((total * checkCoupon.amount) / 100));
            }
            if (checkCoupon.type === "fixed_cart_discount") {
              setDiscountTotalPrice(total - checkCoupon.amount);
              setDiscount(checkCoupon.amount);
            }
          }
        }
      } else {
        setDiscountTotalPrice(0);
      }
    }
  };

  // checkCoupons end

  const useGetAPIData = (query) => {
    let PROJECT_URL = `${query}`;
    useEffect(() => {
      setLoading(true);
      fetch(PROJECT_URL)
        .then((res) => res.json())
        .then((data) => {
          setData(data);
          setLoading(false);
        });
    }, []);
    return data;
  };

  const changeCurrency = (name) => {
    setSiteCurrency(name);
  };

  const getFontInfo = (name, fonts) => {
    const result = fonts.find((font) => font.slug.current == name);
    return result;
  };

  // sorting cart items start

  const groupBy = (list, keyGetter) => {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  };

  const sortCartItems = () => {
    const unsortedMap = groupBy(cartItems, (item) => item.name);
    const unsortedArray = [...unsortedMap];
    const sortedArray = unsortedArray.sort();
    let arraySortedStylePosition = [];
    sortedArray.map((item) => {
      const sorted = item[1].sort((a, b) => a.stylePosition - b.stylePosition);
      const unsortedStyleMap = groupBy(sorted, (itemStyle) => itemStyle.style);
      const unsortedStyleArray = [...unsortedStyleMap];
      const result = unsortedStyleArray.map((itemVariation) => {
        const sortedVariation = itemVariation[1].sort(
          (a, b) => a.variationPosition - b.variationPosition
        );
        return sortedVariation;
      });
      arraySortedStylePosition.push(result);
    });
    return arraySortedStylePosition;
  };

  // sorting cart items end

  const clearCart = () => {
    setOrderId("");
    setCartItems([]);
    setTotalPrice(0);
    setCouponText("");
    setDiscount(0);
    setDiscountTotalPrice(totalPrice);
    window.localStorage.setItem("cartItems", JSON.stringify(cartItems));
    window.localStorage.setItem("totalPrice", JSON.stringify(totalPrice));
    window.localStorage.setItem("couponText", JSON.stringify(couponText));
    window.localStorage.setItem("discount", JSON.stringify(discount));
    window.localStorage.setItem(
      "discountTotalPrice",
      JSON.stringify(discountTotalPrice)
    );
  };

  const changeRadioValue = (setCheckValue, value) => {
    setCheckValue(value);
    document.getElementById(value).checked = true;
  };

  // Validation start

  // Cart validation
  const [shape, setShape] = useState([]);
  shape["acceptTerms"] = Yup.bool().oneOf([true], "");
  const validationSchemaForCart = Yup.object().shape(shape);
  const formOptionsForCart = { resolver: yupResolver(validationSchemaForCart) };
  const { register, handleSubmit, trigger, formState } =
    useForm(formOptionsForCart);
  const { errors } = formState;
  const [checkValue, setCheckValue] = useState("individual");
  const [checkValueLicensee, setCheckValueLicensee] =
    useState("same_as_the_payer");

  // Checkout validation

  const validationSchemaForCheckout = Yup.object().shape({
    company_name:
      checkValue == "company" &&
      Yup.string().required("Company name is required"),
    first_name: Yup.string().required("First name is required"),
    last_name: Yup.string().required("Last name is required"),
    zip_code: Yup.string().required("Zip code is required"),
    address: Yup.string().required("Address is required"),
    email: Yup.string()
      .required("E-mail is required")
      .email("Email is invalid"),
    licensee_name_or_company_name:
      checkValueLicensee == "distinct_from_the_payer" &&
      Yup.string().required("Licensee name or company name is required"),
    tax_payer_id:
      checkValueLicensee == "distinct_from_the_payer" &&
      Yup.string().required("Tax payer id is required"),
  });

  // Check subscribe checkbox in checkout form

  const [isSubscribeNewslette, setIsSubscribeNewslette] = useState(false);
  const isSubscribeNewsletteCheck = () => {
    setIsSubscribeNewslette((current) => !current);
  };

  const payment = () => {
    const params = {
      checkout_url: "https://checkout.bepaid.by",
      checkout: {
        iframe: false,
        test: true,
        transaction_type: "payment",
        public_key: `${process.env.NEXT_PUBLIC_BEPAID_PUBLIC_KEY}`,
        // public_key:
        //   "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0P6Yr3yyvslOr21dKyiXiuvZ4W/PW5n4znfYIOJIXnyHUh6tGO58nOKXbar3Sx3oMALKmVlWJD+FIRYewi1PiYl+dUF1RjAjHsrsPk0GJaqtwHBcjdHjxrRVXXqL7XO2OfgAa5x1Qg08DO71mu/CrQGsIQZVdCbZTfXpA9Hn+dmagD1IumlQ3tfT+V6yCXYsl5YA8dhhPOm7BbZSkOpiZqSHkBwgvQSHZkbXqiQ32XRPXb/hwfFq/CvzeIe/qtcfcjsW/VzEnZ9e2vde3/wT8VYZzOITCVT2zQcFT90x8Sqf2wqDKuzJzeiNERNni4oI2c2kevjZAH/X1eVzN8i+iQIDAQAB",
        order: {
          amount: (discountTotalPrice ? discountTotalPrice : totalPrice) * 100,
          currency: siteCurrency,
          description: "Payment description",
          tracking_id: orderId,
        },
        settings: {
          // save_card_toggle: {
          //   display: false,
          // },
          // success_url: "/payment-complete",
          // decline_url: "/payment-failure",
          language: "ru",
          // style: {
          //   widget: {
          //     backgroundColor: "#dbc21f",
          //     buttonsColor: "#fcf400",
          //     backgroundType: "4",
          //     // backgroundCustomLeft: "https://svgsilh.com/svg/1517090.svg",
          //     // backgroundCustomRight: "https://svgsilh.com/svg/1517090.svg",
          //     fontFamily: "fantasy",
          //     color: "#ff0000",
          //     fontSize: "",
          //     fontSmooth: "",
          //     fontStyle: "",
          //     fontVariant: "",
          //     fontWeight: "",
          //     lineHeight: "",
          //     letterSpacing: "",
          //     textAlign: "",
          //     textDecoration: "",
          //     textShadow: "",
          //     textTransform: "",
          //   },
          //   header: {
          //     display: "block",
          //     shop: {
          //       display: "block",
          //       margin: "",
          //     },
          //     close: {
          //       display: "block",
          //       margin: "",
          //     },
          //   },
          //   price: {
          //     display: "block",
          //     margin: "",
          //   },
          //   description: {
          //     display: "block",
          //     margin: "",
          //   },
          //   button: {
          //     borderRadius: "22px",
          //     backgroundColor: "#03b3ff",
          //     card: {
          //       borderRadius: "22px",
          //       backgroundColor: "#ffffff",
          //       // text: "Pay with card {amount}",
          //       fontFamily: "roboto",
          //       color: "#6f7d79",
          //       fontSize: "",
          //       fontSmooth: "",
          //       fontStyle: "",
          //       fontVariant: "",
          //       fontWeight: "",
          //       lineHeight: "",
          //       letterSpacing: "",
          //       textAlign: "",
          //       textDecoration: "",
          //       textShadow: "",
          //       textTransform: "",
          //     },
          //     brands: {
          //       borderRadius: "22px",
          //       backgroundColor: "#cf8608",
          //       // text: "Pay {amount}",
          //       fontFamily: "consolas",
          //       color: "#2f2cd1",
          //       fontSize: "",
          //       fontSmooth: "",
          //       fontStyle: "",
          //       fontVariant: "",
          //       fontWeight: "",
          //       lineHeight: "",
          //       letterSpacing: "",
          //       textAlign: "",
          //       textDecoration: "",
          //       textShadow: "",
          //       textTransform: "",
          //     },
          //     pay: {
          //       borderRadius: "22px",
          //       backgroundColor: "#24ff03",
          //       // text: "Pay pay final button {amount}",
          //       fontFamily: "calibry",
          //       color: "#bc4ce5",
          //       fontSize: "",
          //       fontSmooth: "",
          //       fontStyle: "",
          //       fontVariant: "",
          //       fontWeight: "",
          //       lineHeight: "",
          //       letterSpacing: "",
          //       textAlign: "",
          //       textDecoration: "",
          //       textShadow: "",
          //       textTransform: "",
          //     },
          //   },
          //   inputs: {
          //     backgroundColor: "",
          //     holder: {
          //       display: "",
          //       backgroundColor: "",
          //     },
          //   },
          //   cardFace: {
          //     backgroundColor: "",
          //     backgroundImage: "",
          //     backgroundPosition: "",
          //     backgroundSize: "",
          //     backgroundRepeat: "",
          //   },
          //   footer: {
          //     display: "block",
          //     logo: {
          //       display: "block",
          //     },
          //   },
          // },
        },
        // customer: {
        //   email: "",
        //   first_name: "",
        //   last_name: "",
        //   address: "",
        //   zip: "",
        //   // country: "",
        // },
      },

      closeWidget: function (status) {
        // возможные значения status
        // successful - операция успешна
        // failed - операция не успешна
        // pending - ожидаем результат/подтверждение операции
        // redirected - пользователь отправлен на внешнюю платежную систему
        // error - ошибка (в параметрах/сети и тд)
        // null - виджет закрыли без запуска оплаты
        console.debug("close widget callback");
      },
    };

    new BeGateway(params).createWidget();
  };

  const onCheckoutSubmit = (data) => {
    window.localStorage.setItem("checkoutFormData", JSON.stringify(data));

    if (isSubscribeNewslette) {
      subscribe();
    }

    onAddEmail();
    payment();
  };

  // Newsletter validation

  const validationSchemaForNewsletter = Yup.object().shape({
    email: Yup.string()
      .required("E-mail is required")
      .email("Email is invalid"),
  });

  // Validation end

  // Send email to Mailchimp start

  const [emailForMailchimp, setEmailForMailchimp] = useState("");
  const [firstNameForMailchimp, setFirstNameForMailchimp] = useState("");
  const [lastNameForMailchimp, setLastNameForMailchimp] = useState("");
  const [state, setState] = useState("idle");
  const [errorMsg, setErrorMsg] = useState(null);

  const subscribe = async (e) => {
    // e.preventDefault();
    setState("Loading");

    try {
      const response = await axios.post("/api/subscribe", {
        emailForMailchimp,
        firstNameForMailchimp,
        lastNameForMailchimp,
      });
      console.log(response);
      setState("Success");
      setEmailForMailchimp("");
      setFirstNameForMailchimp("");
      setLastNameForMailchimp("");
    } catch (e) {
      console.log(e.response.data.error);
      setErrorMsg(e.response.data.error);
      setState("Error");
    }
  };

  // Send email to Mailchimp end

  // Change language start

  const router = useRouter();
  const { locale } = router;
  const t = locale === "ru" ? ru : en;

  const changeLanguage = (locale) => {
    setSiteLang(locale);
    const currentPath = router.asPath;
    router.push(currentPath, currentPath, { locale });
  };

  // Change laguage end

  return (
    <Context.Provider
      value={{
        getMinPriceFont,
        getMinPriceStyle,
        getAmountStyles,
        getPreviewLink,
        cartItems,
        setCartItems,
        onAdd,
        onRemove,
        onUpdate,
        onUpdateInput,
        setFontSlug,
        fontSlug,
        checkCartItems,
        totalPrice,
        setTotalPrice,
        useGetAPIData,
        siteCurrency,
        setSiteCurrency,
        changeCurrency,
        isMobile,
        discount,
        setDiscount,
        discountTotalPrice,
        setDiscountTotalPrice,
        checkCoupons,
        couponText,
        setCouponText,
        couponsArr,
        setCouponsArr,
        getFontInfo,
        sortCartItems,
        clearCart,
        changeRadioValue,
        register,
        errors,
        handleSubmit,
        shape,
        trigger,
        subscribe,
        emailForMailchimp,
        setEmailForMailchimp,
        firstNameForMailchimp,
        lastNameForMailchimp,
        setFirstNameForMailchimp,
        setLastNameForMailchimp,
        state,
        setState,
        checkValue,
        setCheckValue,
        checkValueLicensee,
        setCheckValueLicensee,
        validationSchemaForCheckout,
        onCheckoutSubmit,
        isSubscribeNewslette,
        isSubscribeNewsletteCheck,
        validationSchemaForNewsletter,
        t,
        siteLang,
        changeLanguage,
        locale,
        orderId,
        checkoutFormData,
        cartItemsEmail,
        router,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useStateContext = () => useContext(Context);
