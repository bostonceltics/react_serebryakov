import React from "react";
import Link from "next/link";

import { useStateContext } from "../context/StateContext";

const PaymentFailure = () => {
  const { t } = useStateContext();
  return (
    <>
      <h2
        style={{
          textAlign: `center`,
          fontSize: `28px`,
          lineHeight: `40px`,
          marginBottom: `16px`,
          marginTop: `150px`,
        }}
      >
        {t.payment_failure_title}
      </h2>
      <p style={{ textAlign: `center` }}>
        {`${t.back_to_failure} `}
        <Link href="/cart" className="link-line">
          {t.cart}
        </Link>
      </p>
    </>
  );
};

export default PaymentFailure;
