import React from "react";
import Link from "next/link";

import { useStateContext } from "../context/StateContext";

const NoPage = () => {
  const { t } = useStateContext();

  return (
    <>
      <h2
        style={{
          textAlign: `center`,
          fontSize: `28px`,
          lineHeight: `40px`,
          marginBottom: `16px`,
          marginTop: `150px`,
        }}
      >
        404
      </h2>
      <p style={{ textAlign: `center` }}>
        {`${t.back_to} `}
        <Link href="/" className="link-line">
          {t.fonts}
        </Link>
      </p>
    </>
  );
};

export default NoPage;
