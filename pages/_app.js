import React, { useState, useEffect } from "react";

import { useRouter } from "next/router";
import { Layout } from "../components";
import "../styles/normalize.css";
import "../styles/global.scss";
import { StateContext } from "../context/StateContext";

import Preloader from "../components/Elements/Preloader";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import FooterHidden from "../components/Footer/FooterHidden";

function MyApp({ Component, pageProps }) {
  const [isLoading, setLoading] = useState(false);
  const router = useRouter();

  const handleRouteChangeStart = (url, { shallow }) => {
    setLoading(true);
  };
  const handleRouteChangeComplete = (url, { shallow }) => {
    setTimeout(function () {
      setLoading(false);
    }, 200);
  };

  useEffect(() => {
    router.events.on("routeChangeStart", handleRouteChangeStart);
    router.events.on("routeChangeComplete", handleRouteChangeComplete);
    return () => {
      router.events.off("routeChangeStart", handleRouteChangeStart);
      router.events.off("routeChangeComplete", handleRouteChangeComplete);
    };
  }, [router]);

  return (
    <StateContext>
      <Header />
      <Layout>
        {isLoading && <Preloader />}
        <Component {...pageProps} />
      </Layout>
      <Footer />
      {router.pathname === "/" && <FooterHidden />}
    </StateContext>
  );
}

export default MyApp;
