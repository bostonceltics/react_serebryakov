import React, { useState } from "react";
import Link from "next/link";
import PageTitle from "../components/FontPage/PageTitle";
import CartItem from "../components/CartPage/CartItem";
import Total from "../components/CartPage/Total";
import CheckBox from "../components/Elements/CheckBox";
import Button from "../components/Elements/Button";
import Checkout from "../components/Popups/Checkout";
import { client } from "../lib/client";

import { useStateContext } from "../context/StateContext";

const Cart = ({ coupons, fonts, licensesSanity }) => {
  const { getFontInfo, sortCartItems, handleSubmit, t } = useStateContext();
  const [isShown, setIsShown] = useState(false);

  const showHide = (event) => {
    setIsShown((current) => !current);
  };

  let index = 0;

  const sortedCartItems = sortCartItems();

  function onSubmit(data) {
    setIsShown((current) => !current);
  }

  return (
    <>
      {sortedCartItems.length ? (
        <>
          <PageTitle title="Cart" />

          <form onSubmit={handleSubmit(onSubmit)}>
            {sortedCartItems?.map((item) =>
              item?.map((itemNext) => {
                index = index + 1;
                return (
                  <CartItem
                    key={index}
                    index={index}
                    font={getFontInfo(itemNext[0].name, fonts)}
                    styleName={itemNext[0].style}
                    licenses={itemNext}
                    licensesSanity={licensesSanity}
                  />
                );
              })
            )}

            <Total coupons={coupons} />
            <CheckBox
              id="check"
              text={`${t.terms_text} `}
              linkText={t.terms_text_link}
              link="/licenses"
              name="acceptTerms"
              customClass="checkout_checkbox"
            />
            <Button
              textBtn={t.checkout}
              customClass="largeBtn checkoutBtn"
              link="javascript:void(0);"
              submit={true}
            />
          </form>
          {isShown && <Checkout action={showHide} />}
        </>
      ) : (
        <>
          <h2
            style={{
              textAlign: `center`,
              fontSize: `28px`,
              lineHeight: `40px`,
              marginBottom: `16px`,
              marginTop: `150px`,
            }}
          >
            You added nothing
          </h2>
          <p style={{ textAlign: `center` }}>
            Back to{" "}
            <Link href="/" className="link-line">
              Fonts
            </Link>
          </p>
        </>
      )}
    </>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "coupons"]';
  const coupons = await client.fetch(query);

  const query_fonts = '*[_type == "fonts"]';
  const fonts = await client.fetch(query_fonts);

  const query_licenses = '*[_type == "licenses"]';
  const licensesSanity = await client.fetch(query_licenses);

  return {
    props: { coupons, fonts, licensesSanity },
  };
};

export default Cart;
