import React, { useEffect, useState } from "react";

import { useStateContext } from "../context/StateContext";

const PaymentComplete = () => {
  // const nodemailer = require("nodemailer");
  const {
    clearCart,
    t,
    locale,
    orderId,
    checkoutFormData,
    cartItemsEmail,
    couponText,
    discount,
    totalPrice,
    discountTotalPrice,
  } = useStateContext();

  // useEffect(() => {
  //   clearCart();
  // }, []);

  const currentTime = new Date().toLocaleString("en-US", {
    hour: "numeric",
    minute: "numeric",
    month: "2-digit",
    day: "2-digit",
    year: "2-digit",
  });

  const currentYear = new Date().getFullYear();

  let cartItemsList = "";
  cartItemsEmail.forEach((item, index) => {
    let inputValueString = item.input_value ? `${item.input_value},` : "";
    cartItemsList += `${index + 1}) ${item.font_name}, ${item.font_license}, ${
      item.font_tarif
    }, ${inputValueString} ${item.font_price}\n`;
  });

  const msg_ru = `
  Здравствуйте, ${checkoutFormData.first_name} ${checkoutFormData.last_name}!
  Благодарим вас за покупку. Ниже указаны детали вашего заказа и прикреплены необходимые файлы.

  ЗАКАЗ #${orderId}
  —
  ${cartItemsList}
  —
  ${couponText && `Скидка: ${discount} (промо-код: ${couponText})`}
  Итого: ${discountTotalPrice ? discountTotalPrice : totalPrice}
  —
  Плательщик: ${checkoutFormData.first_name} ${checkoutFormData.last_name}
  ${
    checkoutFormData.company_name &&
    `Компания: ${checkoutFormData.company_name}`
  }
  Страна: ${checkoutFormData.country}
  Индекс: ${checkoutFormData.zip_code}
  Адрес: ${checkoutFormData.address}
  E-mail: ${checkoutFormData.email}
  Лицензиат: ${
    checkoutFormData.licensee_name_or_company_name
      ? checkoutFormData.licensee_name_or_company_name
      : `${checkoutFormData.first_name} ${checkoutFormData.last_name}`
  }
  ${checkoutFormData.tax_payer_id && `ИНН: ${checkoutFormData.tax_payer_id}`}

  —
  Дата: ${currentTime}

  Если у вас есть какие-либо вопросы, технические сложности или хотите оставить свой отзыв, просто ответьте на это письмо, и мы свяжемся с вами в ближайшее время.

  С наилучшими пожеланиями,
  Денис Серебряков.

  <a href="https://serebryakov.com">serebryakov.com</a>
  <a href="https://www.instagram.com/deniserebryakov/">Instagram</a>

  Copyright © ${currentYear} Serebryakov TF

  Вы получили это письмо, так как совершили покупку лицензий Serebryakov TF. Если это произошло по ошибке или вы не понимаете о чем речь, пожалуйста, незамедлительно сообщите нам об этом. Также напоминаем, что соглашаясь с нашим Лицензионным соглашением, вы автоматически подписываетесь на нашу рассылку новостей. Если вы не хотите ее получать, то можете отписаться при первом же получении или сообщите нам о вашем отказе от рассылки прямо сейчас.
  `;

  const msg_en = `
  Hello ${checkoutFormData.first_name} ${checkoutFormData.last_name}!
  Thank you for your purchase. Below are the details of your order and the necessary files attached.

  ORDER #${orderId}
  —
  ${cartItemsList}
  —
  ${couponText && `Discount: ${discount} (promo code: ${couponText})`}
  Total: ${discountTotalPrice ? discountTotalPrice : totalPrice}
  —
  Payer: ${checkoutFormData.first_name} ${checkoutFormData.last_name}
  ${
    checkoutFormData.company_name &&
    `Company name: ${checkoutFormData.company_name}`
  }
  Country: ${checkoutFormData.country}
  Zip code: ${checkoutFormData.zip_code}
  Address: ${checkoutFormData.address}
  E-mail: ${checkoutFormData.email}
  Licensee: ${
    checkoutFormData.licensee_name_or_company_name
      ? checkoutFormData.licensee_name_or_company_name
      : `${checkoutFormData.first_name} ${checkoutFormData.last_name}`
  }
  ${
    checkoutFormData.tax_payer_id &&
    `TaxPayer ID: ${checkoutFormData.tax_payer_id}`
  }

  —
  Date: ${currentTime}

  If you have any questions, issues or feedback, just reply to this email and we will get back to you shortly.

  Best regards,
  Denis Serebryakov.

  <a href="https://serebryakov.com">serebryakov.com</a>
  <a href="https://www.instagram.com/deniserebryakov/">Instagram</a>

  Copyright © ${currentYear} Serebryakov TF

  <i>You have received this letter because you have purchased Serebryakov TF licenses. If this was by mistake or you don't understand what it is about, please let us know immediately. Also a reminder that by agreeing to our License Agreement you automatically subscribe to our newsletter. If you don't want to receive it, you can unsubscribe the first time you receive it, or let us know that you are unsubscribing right now.</i>
  `;

  // const transporter = nodemailer.createTransport({
  //     host: process.env.NODEMAILER_HOST,
  //     port: 465,
  //     secure: true,
  //     auth: {
  //       user: process.env.NODEMAILER_USER,
  //       pass: process.env.NODEMAILER_PASSWORD,
  //     },
  //   });

  //   const message = {
  //     from: "Serebryakov TF <fonts@serebryakov.com>",
  //     to: checkoutFormData.email,
  //     subject: locale === 'ru' ? `Уведомление о покупке #${orderId}` : `Purchase notice #${orderId}`,
  //     html: locale === 'ru' ? msg_ru : msg_en,
  //     attachments: [
  //       {
  //         filename: "serebryakovtf_web_license.pdf",
  //         path: "https://cdn.sanity.io/files/vfp5mycn/production/8cb1cfa47dfb067e5e292d17081a9bbebd02751c.pdf",
  //       },
  //     ],
  //   };

  //   transporter.sendMail(message, (error, info) => {
  //     if (error) {
  //       res.status(500).json({ error: error });
  //     } else {
  //       res.status(200).json({ respose: `Email sent: ${info.response}` });
  //       clearCart();
  //     }
  //   });

  return (
    <>
      <h2
        style={{
          textAlign: `center`,
          fontSize: `28px`,
          lineHeight: `40px`,
          marginBottom: `16px`,
          marginTop: `150px`,
        }}
      >
        {t.payment_compleate_title}
      </h2>
      <p style={{ textAlign: `center` }}>{t.payment_compleate_desc}</p>
    </>
  );
};

export default PaymentComplete;
