import React from "react";
import PageTitle from "../components/FontPage/PageTitle";
import InfoPage from "../components/InfoPage/InfoPage";
import { client } from "../lib/client";

const Info = ({ info }) => {
  return (
    <>
      <PageTitle title="Info" />
      <InfoPage info={info} />
    </>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "info"]';
  const info = await client.fetch(query);

  return {
    props: { info },
  };
};

export default Info;
