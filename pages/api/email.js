const nodemailer = require("nodemailer");

export default function handler(req, res) {
  const { locale } = req.body;
  // const msg_ru = `
  // Здравствуйте, ${first_name} ${last_name}!
  // Благодарим вас за покупку. Ниже указаны детали вашего заказа и прикреплены необходимые файлы.

  // ЗАКАЗ #${order_number}
  // —
  // ${order?.map(item, (index) => {
  //   return `${index}) ${font_name}, ${font_license}, ${font_tarif}, ${font_price}`;
  // })}
  // —
  // Скидка: ${discount_price} (промо-код: ${discount_text})
  // Итого: ${total_price}
  // —
  // Плательщик: ${first_name} ${last_name}
  // ${company_name && `Компания: ${company_name}`}
  // Страна: ${country}
  // Индекс: ${zip_code}
  // Адрес: ${address}
  // E-mail: ${email}
  // Лицензиат: ${first_name} ${last_name}
  // ИНН: ${taxpayer_id}
  // —
  // Дата: ${data_time} !!!16/12/2022, 09:05!!!

  // Если у вас есть какие-либо вопросы, технические сложности или хотите оставить свой отзыв, просто ответьте на это письмо, и мы свяжемся с вами в ближайшее время.

  // С наилучшими пожеланиями,
  // Денис Серебряков.

  // <a href="https://serebryakov.com">serebryakov.com</a>
  // <a href="https://www.instagram.com/deniserebryakov/">Instagram</a>

  // Copyright © ${date_year} Serebryakov TF

  // Вы получили это письмо, так как совершили покупку лицензий Serebryakov TF. Если это произошло по ошибке или вы не понимаете о чем речь, пожалуйста, незамедлительно сообщите нам об этом. Также напоминаем, что соглашаясь с нашим Лицензионным соглашением, вы автоматически подписываетесь на нашу рассылку новостей. Если вы не хотите ее получать, то можете отписаться при первом же получении или сообщите нам о вашем отказе от рассылки прямо сейчас.
  // `;

  // const msg_en = `
  // Hello ${first_name} ${last_name}!
  // Thank you for your purchase. Below are the details of your order and the necessary files attached.

  // ORDER #${order_number}
  // —
  // ${order?.map(item, (index) => {
  //   return `${index}) ${font_name}, ${font_license}, ${font_tarif}, ${font_price}`;
  // })}
  // —
  // Discount: ${discount_price} (promo code: ${discount_text})
  // Total: ${total_price}
  // —
  // Payer: ${first_name} ${last_name}
  // ${company_name && `Company name: ${company_name}`}
  // Country: ${country}
  // Zip code: ${zip_code}
  // Address: ${address}
  // E-mail: ${email}
  // ${
  //   licensee
  //     ? `Licensee: ${first_name} ${last_name}`
  //     : `Licensee: ${licensee_name}
  //     TaxPayer ID: ${taxpayer_id}`
  // }
  // —
  // Date: ${data_time} !!!16/12/2022, 09:05!!!

  // If you have any questions, issues or feedback, just reply to this email and we will get back to you shortly.

  // Best regards,
  // Denis Serebryakov.

  // <a href="https://serebryakov.com">serebryakov.com</a>
  // <a href="https://www.instagram.com/deniserebryakov/">Instagram</a>

  // Copyright © ${date_year} Serebryakov TF

  // <i>You have received this letter because you have purchased Serebryakov TF licenses. If this was by mistake or you don't understand what it is about, please let us know immediately. Also a reminder that by agreeing to our License Agreement you automatically subscribe to our newsletter. If you don't want to receive it, you can unsubscribe the first time you receive it, or let us know that you are unsubscribing right now.</i>
  // `;

  // const transporter = nodemailer.createTransport({
  //   host: process.env.NODEMAILER_HOST,
  //   port: 465,
  //   secure: true,
  //   auth: {
  //     user: process.env.NODEMAILER_USER,
  //     pass: process.env.NODEMAILER_PASSWORD,
  //   },
  // });

  // const message = {
  //   from: "Serebryakov TF <fonts@serebryakov.com>",
  //   to: email,
  //   subject: "Purchase notice #14949", // Уведомление о покупке #14971
  //   html: msg_en,
  //   attachments: [
  //     {
  //       filename: "serebryakovtf_web_license.pdf",
  //       path: "https://cdn.sanity.io/files/vfp5mycn/production/8cb1cfa47dfb067e5e292d17081a9bbebd02751c.pdf",
  //     },
  //   ],
  // };

  // transporter.sendMail(message, (error, info) => {
  //   if (error) {
  //     res.status(500).json({ error: error });
  //   } else {
  //     res.status(200).json({ respose: `Email sent: ${info.response}` });
  //   }
  // });
  res.status(200).json({ respose: `Email sent: ${res.locale}` });
}
