import React from "react";
import PageTitle from "../components/FontPage/PageTitle";
import { client } from "../lib/client";
import { PortableText } from "@portabletext/react";

import { useStateContext } from "../context/StateContext";

const Credentials = ({ page }) => {
  const { locale } = useStateContext();
  return (
    <>
      <PageTitle title={page[`title_${locale}`]} />

      <div className="grid-50">
        <PortableText value={page[`content_${locale}`]} />
      </div>
    </>
  );
};

export const getServerSideProps = async () => {
  const query = `*[_type == "pages" && slug.current == 'credentials'][0]`;
  const page = await client.fetch(query);

  return {
    props: { page },
  };
};

export default Credentials;
