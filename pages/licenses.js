import React, { useState } from "react";
import PageTitle from "../components/FontPage/PageTitle";
import Table from "../components/LicensesPage/Table";
import { client } from "../lib/client";

import { useStateContext } from "../context/StateContext";

const Licenses = ({ faq, licenses }) => {
  const { t } = useStateContext();

  const [isShowFaq, setIsShowFaq] = useState(false);
  const [isShowLicenses, setIsShowLicenses] = useState(true);

  const showFaq = () => {
    setIsShowFaq(true);
    setIsShowLicenses(false);
  };
  const showLicenses = () => {
    setIsShowLicenses(true);
    setIsShowFaq(false);
  };
  return (
    <>
      <div className="wrapRow">
        <PageTitle
          title={t.page_title_licenses}
          className={isShowLicenses ? "underline" : ""}
          show={showLicenses}
        />
        <PageTitle
          title={t.page_title_faq}
          className={isShowFaq ? "underline" : ""}
          show={showFaq}
        />
      </div>
      <Table
        type={isShowFaq ? "faq" : "licenses"}
        data={isShowFaq ? faq : licenses}
      />
    </>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "faq"]';
  const faq = await client.fetch(query);

  const query_licenses = '*[_type == "licenses"]';
  const licenses = await client.fetch(query_licenses);

  return {
    props: { faq, licenses },
  };
};

export default Licenses;
