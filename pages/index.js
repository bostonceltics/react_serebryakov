import { useEffect } from "react";
import { client } from "../lib/client";
import HomeFont from "../components/HomeFont/HomeFont";

import { useStateContext } from "../context/StateContext";

const Home = ({ fonts, countryCode }) => {
  const {
    getMinPriceFont,
    getAmountStyles,
    getPreviewLink,
    changeCurrency,
    changeLanguage,
    siteLang,
  } = useStateContext();

  useEffect(() => {
    console.log(countryCode);
    if (!window.localStorage.getItem("siteLang")) {
      if (countryCode === "RU") {
        changeCurrency("RUB");
        changeLanguage("ru");
      } else if (countryCode === "BY") {
        changeCurrency("BYN");
        changeLanguage("ru");
      } else {
        changeCurrency("USD");
        changeLanguage("en");
      }
    }
  }, [countryCode, siteLang]);

  return (
    <>
      {fonts?.map((font) => (
        <HomeFont
          key={font._id}
          name={font.title}
          slug={font.slug.current}
          preview={font.preview ? getPreviewLink(font.preview.asset._ref) : ""}
          amount={getAmountStyles(font.fonts)}
          minPrice={getMinPriceFont(font.fonts)}
        />
      ))}
    </>
  );
};

export const getServerSideProps = async (context) => {
  const query = '*[_type == "fonts"]';
  const fonts = await client.fetch(query);

  const countryCode = context.req.headers["cf-ipcountry"] || null;

  return {
    props: { fonts, countryCode },
  };
};

export default Home;
