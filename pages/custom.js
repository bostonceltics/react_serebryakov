import React from "react";
import PageTitle from "../components/FontPage/PageTitle";
import CustomSlider from "../components/CustomPage/CustomSlider";
import CustomLogos from "../components/CustomPage/CustomLogos";
import { client } from "../lib/client";

import { useStateContext } from "../context/StateContext";

const Custom = ({ custom, logotypes }) => {
  const { t } = useStateContext();
  return (
    <>
      <PageTitle title={t.page_title_custom} />
      <div className="wrapRow">
        {custom?.map((item, index) => (
          <CustomSlider key={index} data={item} />
        ))}
      </div>

      <CustomLogos logotypes={logotypes} />
    </>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "Custom"]';
  const custom = await client.fetch(query);

  const query_logo = '*[_type == "logo"]';
  const logotypes = await client.fetch(query_logo);

  return {
    props: { custom, logotypes },
  };
};

export default Custom;
