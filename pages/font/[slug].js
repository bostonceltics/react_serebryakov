import React from "react";
import { client, urlFor } from "../../lib/client";
import PageTitle from "../../components/FontPage/PageTitle";
import FontItem from "../../components/FontPage/FontItem";
import Symbols from "../../components/FontPage/Symbols";

import { useStateContext } from "../../context/StateContext";

const FontPage = ({ font, licensesSanity }) => {
  const { title, family_prices, fonts, slug } = font;
  const { getAmountStyles, getPreviewLink, setFontSlug } = useStateContext();

  const lowercase = font.lowercase;
  setFontSlug(`${font.slug.current}`);

  return (
    <>
      <PageTitle title={title} />
      {family_prices && (
        <FontItem
          key={0}
          family={family_prices ? true : false}
          title={title}
          amount={getAmountStyles(fonts)}
          fonts={fonts}
          family_prices={family_prices}
          index={0}
          licensesSanity={licensesSanity}
        />
      )}
      {fonts?.map((font, index) => (
        <FontItem
          key={index + 1}
          name={font.name}
          prices={font.prices}
          word={font.word}
          lowercase={lowercase}
          style={font.style}
          styleLink={font.font.asset._ref}
          index={index + 1}
          licensesSanity={licensesSanity}
        />
      ))}

      <Symbols
        italic={font.italic}
        previewLink={
          font.preview ? getPreviewLink(font.preview.asset._ref) : ""
        }
        fontStyle={`${font.slug.current}`}
      />
    </>
  );
};

export const getStaticPaths = async () => {
  const query = `*[_type == "fonts"] {
      slug {
        current
      }
    }
    `;

  const fonts = await client.fetch(query);

  const paths = fonts.map((font) => ({
    params: {
      slug: font.slug.current,
    },
  }));

  return {
    paths,
    fallback: "blocking",
  };
};

export const getStaticProps = async ({ params: { slug } }) => {
  const query = `*[_type == "fonts" && slug.current == '${slug}'][0]`;
  const query_licenses = '*[_type == "licenses"]';

  const font = await client.fetch(query);
  const licensesSanity = await client.fetch(query_licenses);

  return {
    props: { font, licensesSanity },
  };
};

export default FontPage;
