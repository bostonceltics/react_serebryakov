import React from "react";
import Link from "next/link";
import styles from "./HomeFont.module.scss";
import Style from "../Elements/Style";
import Price from "../Elements/Price";

import { useStateContext } from "../../context/StateContext";

const HomeFont = ({ name, slug, amount, minPrice, preview }) => {
  const { t, locale } = useStateContext();
  return (
    <>
      {preview && <Style slug={slug} preview={preview} />}
      <div className={styles.item}>
        <Link
          href={`/font/${slug}`}
          className={styles.title}
          style={{ fontFamily: `${slug}, sans-serif` }}
        >
          {name}
        </Link>

        <div className={styles.info}>
          <Link href={`/font/${slug}`} className="link-line">
            {name}
          </Link>
          <p>
            , {amount}{" "}
            {amount === 1
              ? t.style
              : (amount === 2 || amount === 3 || amount === 4) &&
                locale === "ru"
              ? "начертания"
              : t.styles}
            , <Price price={minPrice} min={true} />
          </p>
        </div>
      </div>
    </>
  );
};

export default HomeFont;
