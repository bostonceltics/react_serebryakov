import React, { useEffect, useRef } from "react";

import styles from "./FooterHidden.module.scss";
import { useStateContext } from "../../context/StateContext";

const FooterHidden = () => {
  const { t } = useStateContext();
  const hiddenRef = useRef();

  useEffect(() => {
    window.addEventListener("scroll", scrollHandler);
    return () => window.removeEventListener("scroll", scrollHandler);
  }, []);

  let show = 0;
  const scrollHandler = () => {
    const footerHidden = document.querySelector("#footerHidden");

    if (
      window.pageYOffset + window.innerHeight >=
      hiddenRef.current.offsetTop
    ) {
      if (show == 0) {
        setTimeout(function () {
          footerHidden.style.display = "flex";
          show = 1;
        }, 2500);
      }
    } else {
      footerHidden.style.display = "none";
      show = 0;
    }
  };

  return (
    <div ref={hiddenRef} id="footerHidden" className={styles.container}>
      <div className={styles.line}></div>
      <div className={styles.images}>
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-1.svg"
          alt=""
        />{" "}
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-2.svg"
          alt=""
        />
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-3.png"
          style={{ maxWidth: `83px` }}
          alt=""
        />
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-4.png"
          style={{ maxWidth: `40px` }}
          alt=""
        />{" "}
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-5.svg"
          alt=""
        />
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-6.svg"
          alt=""
        />{" "}
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-7.png"
          style={{ maxWidth: `52px` }}
          alt=""
        />
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-8.svg"
          alt=""
        />{" "}
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-9.png"
          style={{ maxWidth: `65px` }}
          alt=""
        />
        <img
          src="https://serebryakov.com/wp-content/themes/dizzain-new/images/new-footer/pay-10.png"
          style={{ maxWidth: `57px` }}
          alt=""
        />
      </div>
      {<p dangerouslySetInnerHTML={{ __html: t.footer_copy }} />}
    </div>
  );
};

export default FooterHidden;
