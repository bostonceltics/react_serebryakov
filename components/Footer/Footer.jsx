import React, { useState } from "react";
import Link from "next/link";
import Newsletter from "../Popups/Newsletter";

import styles from "./Footer.module.scss";

import { useStateContext } from "../../context/StateContext";

const Footer = () => {
  const { t } = useStateContext();

  const today = new Date();
  const [isShown, setIsShown] = useState(false);

  const showHide = (event) => {
    setIsShown((current) => !current);
  };

  return (
    <>
      <footer className={styles.container}>
        <p>&copy; 2006–{today.getFullYear()}, Serebryakov</p>
        <ul className={styles.list}>
          <li>
            <Link
              href="https://www.instagram.com/deniserebryakov/"
              className="link"
              target="_blank"
            >
              Instagram
            </Link>
          </li>
          <li>
            <Link
              href="javascript:void(0);"
              className="link"
              onClick={showHide}
            >
              {t.newsletter}
            </Link>
          </li>
        </ul>
      </footer>
      {isShown && <Newsletter action={showHide} />}
    </>
  );
};

export default Footer;
