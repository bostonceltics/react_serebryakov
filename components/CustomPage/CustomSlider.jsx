import React from "react";
import { urlFor } from "../../lib/client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";

import styles from "./CustomSlider.module.scss";

import { useStateContext } from "../../context/StateContext";

const CustomSlider = ({ data }) => {
  const { locale } = useStateContext();
  var settings = {
    infinite: true,
    dots: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div className={styles.item}>
      <p>{data[`title_${locale}`]}</p>
      <p>{data[`desc_${locale}`]}</p>
      <Slider {...settings}>
        {data.gallery?.map((item) => (
          <>
            <Image
              loader={() => urlFor(item).url()}
              src={urlFor(item).url()}
              alt="custom-project"
              width={500}
              height={500}
            />
          </>
        ))}
      </Slider>
    </div>
  );
};

export default CustomSlider;
