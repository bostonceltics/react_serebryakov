import React from "react";
import styles from "./CustomLogos.module.scss";
import Image from "next/image";
import { urlFor } from "../../lib/client";

const CustomLogos = ({ logotypes }) => {
  return (
    <div className={styles.grid}>
      {logotypes[0].image?.map((item) => (
        <>
          <Image
            loader={() => urlFor(item).url()}
            src={urlFor(item).url()}
            alt="custom-project"
            width={500}
            height={500}
          />
        </>
      ))}
    </div>
  );
};

export default CustomLogos;
