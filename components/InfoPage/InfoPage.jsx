import React from "react";
import Link from "next/link";
import styles from "./InfoPage.module.scss";

const InfoPage = ({ info }) => {
  // const content = info[0].content_en[1].code;
  return (
    <div class={styles.grid}>
      <div class={styles.col}>
        <h2>
          Retail &amp; custom fonts production, design logotypes and projects
          with a focus on typography
        </h2>
        <div class={styles.gridLeft}>
          <div>
            <h3>CONTACTS</h3>
            <p>
              Denis Serebryakov is a belarusian graphic and type designer,
              author of the world-famous Appetite font and dozens of logotypes.
            </p>
            <p>
              For creative collaborations, trial font licenses or special prices
              for companies, feel free to contact me.
            </p>
            <p>
              <Link href="tel:+375296933023" className="link-line">
                +375 (29) 693-30-23
              </Link>
              <br />
              <Link href="mailto:fonts@serebryakov.com" className="link-line">
                fonts@serebryakov.com
              </Link>
            </p>
            <p>
              <Link href="/credentials" className="link-line">
                Credentials
              </Link>
              <br />
              <Link href="/payments-refunds" className="link-line">
                Payments &amp; Refunds
              </Link>
              <br />
              <Link href="/privacy-policy" className="link-line">
                Privacy Policy
              </Link>
            </p>
            <div class={styles.spacer}></div>
            <h3>FONT USERS</h3>
            <p>
              ATypi 2017
              <br /> Royal National Theater <br />
              PepsiCo Inc. (Lays Brand)
              <br />
              Nestle <br />
              The Reykjavik Grapevine
              <br /> Megafon
              <br /> Mareven (Rollton Brand)
              <br />
              Doshirak
              <br /> Savushkin
            </p>
          </div>
          <div>
            <h3>SERVICES</h3>
            <p>
              Font from Logo
              <br />A simple way to get a corporate font. Brand communication
              will be more effective, because the logo will be in every letter.
            </p>
            <p>
              Font Modification <br />
              You can order a customization of any of our fonts. We will make
              the necessary changes to make the font special and integrate with
              your brand or location.
            </p>
            <p>
              New Font Design <br />
              Development of a font based on the specific characteristics of
              your brand or marketing goals.
            </p>
            <div class={styles.spacer}></div>
            <h3>MENTIONS</h3>
            <p>
              <Link
                href="https://www.granshan.com/competition-winners-2021-22"
                className="link-line"
              >
                12th Granshan Type Design Competition 2022
              </Link>
              , Special Mention Cyrillic typeface category, Nekst <br />«
              <Link href="https://moderncyrillic.org" className="link-line">
                Modern Cyrillic Competition 2021
              </Link>
              », Winner, 
              <Link
                href="https://serebryakov.com/ru/font/epos/"
                className="link-line"
              >
                Epos
                <br />
              </Link>
              Type.Today,
              <Link
                href="https://type.today/ru/journal/serebryakov"
                className="link-line"
              >
                Interview
              </Link>
               with Denis Serebryakov, 2020 <br />«
              <Link href="https://moderncyrillic.org" className="link-line">
                Современная кириллица
              </Link>
              », Jury member of the contest, 2019 <br />
              Shrift Magazine:
              <Link
                href="http://2017.typejournal.ru/rozza/"
                className="link-line"
              >
                Best Cyrillic typefaces of the 2017
              </Link>
              ,
              <Link
                href="https://serebryakov.com/ru/font/rozza/"
                className="link-line"
              >
                Rozza
                <br />
              </Link>
              Shrift Magazine:
              <Link
                href="https://typejournal.ru/articles/The-Best-Cyrillic-Typefaces-2014"
                className="link-line"
              >
                Best Cyrillic typefaces of the 2014
              </Link>
              ,
              <Link
                href="https://serebryakov.com/ru/font/bouquet/"
                className="link-line"
              >
                Bouquet
                <br />
              </Link>
              Typographica.org, 
              <Link
                href="http://typographica.org/typeface-reviews/bouquet/"
                className="link-line"
              >
                Typeface Review of the 2014
              </Link>
              ,
              <Link
                href="https://serebryakov.com/ru/font/bouquet/"
                className="link-line"
              >
                Bouquet
              </Link>
            </p>
          </div>
        </div>
      </div>

      <img
        src="https://bsb.serebryakov.com/wp-content/uploads/2022/10/dscf2706-scaled.jpeg"
        alt=""
      />
    </div>
  );
};

export default InfoPage;
