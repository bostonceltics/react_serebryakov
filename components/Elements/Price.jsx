import React, { useState, useEffect } from "react";
import { client } from "../../lib/client";
import { useStateContext } from "../../context/StateContext";

const Price = ({ price, symbol, min, checkout }) => {
  const { siteCurrency, t } = useStateContext();
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [dataCurrency, setDataCurrency] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const result = await client.fetch('*[_type == "currency"]');
      setDataCurrency(result);
    };

    fetchData();
  }, []);

  const useGetAPIData = (query) => {
    let PROJECT_URL = `${query}`;
    useEffect(() => {
      setLoading(true);
      fetch(PROJECT_URL)
        .then((res) => res.json())
        .then((data) => {
          setData(data);
          setLoading(false);
        });
    }, []);
    return data;
  };

  let BANK_URL = `https://www.nbrb.by/api/exrates/rates?periodicity=0`;
  let result = useGetAPIData(BANK_URL);
  let dataBank = [];
  let RUBRate;
  let USDRate;
  if (result) {
    dataBank = result;
    RUBRate = dataBank.find((item) => item.Cur_ID === 456);
    USDRate = dataBank.find((item) => item.Cur_ID === 431);
  }
  const currency = siteCurrency;
  let newPrice = price;

  if (result && currency == "BYN") {
    newPrice = price * USDRate.Cur_OfficialRate;
    newPrice = Math.round(newPrice / 5) * 5;
    newPrice = new Intl.NumberFormat("ru-RU", {
      style: `${symbol === "false" ? "decimal" : "currency"}`,
      currency: "BYN",
      currencyDisplay: "code",
      minimumFractionDigits: 0,
    }).format(newPrice);
  }
  if (result && currency == "RUB") {
    newPrice =
      ((price * USDRate.Cur_OfficialRate) / RUBRate.Cur_OfficialRate) * 100;
    newPrice = Math.round(newPrice / 5) * 5;
    newPrice = new Intl.NumberFormat("ru-RU", {
      style: `${symbol === "false" ? "decimal" : "currency"}`,
      currency: "RUB",
      currencyDisplay: "symbol",
      minimumFractionDigits: 0,
    }).format(newPrice);
  }
  if (currency == "USD") {
    newPrice = new Intl.NumberFormat("en-US", {
      style: `${symbol === "false" ? "decimal" : "currency"}`,
      currency: "USD",
      currencyDisplay: "symbol",
      minimumFractionDigits: 0,
    })
      .format(newPrice)
      .replaceAll(",", " ");
  }

  if (min === true) {
    newPrice = `${t.from} ${newPrice}`;
  }
  if (checkout === true) {
    newPrice = `${t.popup_pay} ${newPrice}`;
  }
  return <>{newPrice}</>;
};

export default Price;
