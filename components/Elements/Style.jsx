import React from "react";
import Head from "next/head";

const Style = ({ slug, preview }) => {
  return (
    <Head>
      <style>
        {`
    @font-face {
        font-family: ${slug};
        src: url(${preview}) format('woff');
        font-weight: 400;
        font-style: normal;
      }`}
      </style>
    </Head>
  );
};

export default Style;
