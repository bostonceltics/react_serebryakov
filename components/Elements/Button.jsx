import React from "react";
import Link from "next/link";
import styles from "./Button.module.scss";
import { TfiClose } from "react-icons/tfi";

const Button = ({
  textBtn,
  link,
  customClass,
  showHide,
  isShown,
  onAdd,
  submit,
  state,
}) => {
  let action;
  if (showHide) {
    action = showHide;
  }
  if (onAdd) {
    action = onAdd;
  }

  return (
    <>
      {!submit ? (
        <>
          {isShown && (
            <TfiClose
              size={22}
              onClick={action}
              style={{ cursor: "pointer", position: "absolute", right: "0" }}
            />
          )}

          <Link
            href={link}
            className={`${styles.btn} ${customClass ? customClass : ``}`}
            onClick={action}
            style={{
              opacity: `${isShown ? "0" : "1"}`,
              pointerEvents: `${isShown ? "none" : ""}`,
            }}
          >
            {textBtn}
          </Link>
        </>
      ) : (
        <button
          type="submit"
          disabled={state === "Loading"}
          className={`${styles.btn} ${customClass ? customClass : ``}`}
        >
          {textBtn}
        </button>
      )}
    </>
  );
};

export default Button;
