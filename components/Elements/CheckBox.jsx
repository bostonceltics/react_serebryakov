import React from "react";
import Link from "next/link";
import styles from "./CheckBox.module.scss";
import { useStateContext } from "../../context/StateContext";

const CheckBox = ({ id, name, link, linkText, text, customClass, action }) => {
  const { register, errors } = useStateContext();
  return (
    <label
      htmlFor={id}
      onChange={action}
      className={`${styles.item} ${customClass} ${
        errors[name] && "is-invalid-color"
      }`}
    >
      <input type="checkbox" name={name} id={id} {...register(name)} />
      <span>{text}</span>
      {linkText && (
        <Link href={link} className="link-line" target="_blank">
          {linkText}
        </Link>
      )}
    </label>
  );
};

export default CheckBox;
