import React from "react";
import styles from "./RadioButton.module.scss";

const RadioButton = ({ id, name, text }) => {
  return (
    <label htmlFor={id} className={styles.radio}>
      <input type="radio" name={name} id={id} value={id} />
      <span>{text}</span>
    </label>
  );
};

export default RadioButton;
