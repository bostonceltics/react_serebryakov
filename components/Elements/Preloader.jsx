import React from "react";
import styles from "./Preloader.module.scss";
import Image from "next/image";
import Spin from "../../images/spin.gif";

const Preloader = () => {
  return (
    <div className={styles.container}>
      <Image src={Spin} alt="spin" width={40} height={40} />
    </div>
  );
};

export default Preloader;
