import React from "react";
import Link from "next/link";
import { CiCircleInfo } from "react-icons/ci";
import styles from "./Hint.module.scss";

import { useStateContext } from "../../context/StateContext";

const Hint = ({ text, link }) => {
  const { t } = useStateContext();
  return (
    <div className={styles.wrap}>
      <CiCircleInfo size={25} className={styles.icon} />
      <div className={styles.blockWrap}>
        <div className={styles.block}>
          <p>{text}</p>
          <Link href={link} className="link-line" target="_blank">
            {t.more}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Hint;
