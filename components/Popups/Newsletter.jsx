import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { TfiClose } from "react-icons/tfi";
import Button from "../Elements/Button";
import styles from "./Popup.module.scss";
import { useStateContext } from "../../context/StateContext";

const Newsletter = ({ action }) => {
  const {
    subscribe,
    emailForMailchimp,
    setEmailForMailchimp,
    setState,
    state,
    validationSchemaForNewsletter,
    t,
  } = useStateContext();

  const formOptions = { resolver: yupResolver(validationSchemaForNewsletter) };

  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  if (state === "Success") {
    setTimeout(function () {
      setState("");
      setEmailForMailchimp("");
    }, 2000);
  }

  return (
    <>
      <div className={styles.layout} onClick={action}></div>
      <div
        id="subscibed"
        className={`${styles.popup} ${state === "Success" && styles.success} `}
      >
        {state === "Success" ? (
          <h2>{t.popup_subscribed}</h2>
        ) : (
          <>
            <div className={styles.top}>
              <p>{t.newsletter}</p>
              <TfiClose
                size={22}
                onClick={action}
                style={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={handleSubmit(subscribe)}>
              <input
                type="email"
                name="email"
                placeholder="E-mail"
                {...register("email")}
                className={`${
                  errors.email || state === "Error" ? "is-invalid-color" : ""
                }`}
                value={emailForMailchimp}
                onChange={(e) => setEmailForMailchimp(e.target.value)}
              />
              <Button
                textBtn={t.subscribe_btn}
                customClass="largeBtn popupBtn"
                link="javascript:void(0);"
                state={state}
                submit={true}
              />
            </form>
          </>
        )}
      </div>
    </>
  );
};

export default Newsletter;
