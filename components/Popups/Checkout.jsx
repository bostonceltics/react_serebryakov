import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { TfiClose } from "react-icons/tfi";
import { IoMdArrowDropdown } from "react-icons/io";
import Button from "../Elements/Button";
import Price from "../Elements/Price";
import RadioButton from "../Elements/RadioButton";
import CheckBox from "../Elements/CheckBox";
import styles from "./Popup.module.scss";

import { useStateContext } from "../../context/StateContext";

const Checkout = ({ action }) => {
  const {
    totalPrice,
    discountTotalPrice,
    changeRadioValue,
    emailForMailchimp,
    setEmailForMailchimp,
    firstNameForMailchimp,
    lastNameForMailchimp,
    setFirstNameForMailchimp,
    setLastNameForMailchimp,
    state,
    checkValue,
    setCheckValue,
    checkValueLicensee,
    setCheckValueLicensee,
    validationSchemaForCheckout,
    onCheckoutSubmit,
    isSubscribeNewsletteCheck,
    t,
  } = useStateContext();

  const formOptions = { resolver: yupResolver(validationSchemaForCheckout) };

  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  useEffect(() => {
    changeRadioValue(setCheckValue, checkValue);
    changeRadioValue(setCheckValueLicensee, checkValueLicensee);
  }, []);

  return (
    <>
      <div className={styles.layout} onClick={action}></div>
      <div className={styles.popupLayout}>
        <div className={`${styles.popup} checkout_popup`}>
          <div className={styles.top}>
            <p>{t.checkout}</p>
            <TfiClose
              size={22}
              onClick={action}
              style={{ cursor: "pointer" }}
            />
          </div>
          <form onSubmit={handleSubmit(onCheckoutSubmit)}>
            <p className={`${styles.subtitle} uppercase`}>{t.popup_payer}</p>
            <div className={styles.block}>
              <div
                className={styles.nav}
                onChange={(e) =>
                  changeRadioValue(setCheckValue, e.target.value)
                }
              >
                <RadioButton
                  id="individual"
                  name="payer_type"
                  text={t.popup_individual}
                />
                <RadioButton
                  id="company"
                  name="payer_type"
                  text={t.popup_company}
                />
              </div>
              {checkValue == "company" && (
                <input
                  type="text"
                  name="company_name"
                  placeholder={t.popup_company_name}
                  {...register("company_name")}
                  className={`${styles.input100} ${
                    errors.company_name ? "is-invalid" : ""
                  }`}
                />
              )}

              <input
                type="text"
                name="first_name"
                placeholder={t.popup_first_name}
                {...register("first_name")}
                className={`${styles.input50} ${
                  errors.first_name ? "is-invalid" : ""
                }`}
                value={firstNameForMailchimp}
                onChange={(e) => setFirstNameForMailchimp(e.target.value)}
              />

              <input
                type="text"
                name="last_name"
                placeholder={t.popup_last_name}
                {...register("last_name")}
                className={`${styles.input50} ${
                  errors.last_name ? "is-invalid" : ""
                }`}
                value={lastNameForMailchimp}
                onChange={(e) => setLastNameForMailchimp(e.target.value)}
              />
              <div className={styles.input50}>
                <select name="country" value="belarus">
                  <option value="belarus">Belarus</option>
                  <option value="russia">Russia</option>
                  <option value="usa">USA</option>
                </select>
                <IoMdArrowDropdown size={25} />
              </div>

              <input
                type="text"
                name="zip_code"
                placeholder={t.popup_zip_code}
                {...register("zip_code")}
                className={`${styles.input50} ${
                  errors.zip_code ? "is-invalid" : ""
                }`}
              />
              <input
                type="text"
                name="address"
                placeholder={t.popup_address}
                {...register("address")}
                className={`${styles.input100} ${
                  errors.address ? "is-invalid" : ""
                }`}
              />
              <input
                type="text"
                name="email"
                placeholder={t.popup_email}
                {...register("email")}
                className={`${styles.input100} ${
                  errors.email ? "is-invalid" : ""
                }`}
                value={emailForMailchimp}
                onChange={(e) => setEmailForMailchimp(e.target.value)}
              />
              <CheckBox
                name="subscribe_newslette"
                text={t.popup_subscribe_newslette}
                customClass="popup_checkbox"
                action={isSubscribeNewsletteCheck}
              />
            </div>

            <p className={`${styles.subtitle} uppercase`}>{t.popup_licensee}</p>
            <div className={styles.block}>
              <div
                className={styles.nav}
                onChange={(e) =>
                  changeRadioValue(setCheckValueLicensee, e.target.value)
                }
              >
                <RadioButton
                  id="same_as_the_payer"
                  name="licensee_type"
                  text={t.popup_same_as_the_payer}
                  checked={true}
                />
                <RadioButton
                  id="distinct_from_the_payer"
                  name="licensee_type"
                  text={t.popup_distinct_from_the_payer}
                  checked={false}
                />
              </div>
              {checkValueLicensee == "distinct_from_the_payer" && (
                <>
                  <input
                    type="text"
                    name="licensee_name_or_company_name"
                    placeholder={t.popup_licensee_name_or_company_name}
                    {...register("licensee_name_or_company_name")}
                    className={`${styles.input100} ${
                      errors.licensee_name_or_company_name ? "is-invalid" : ""
                    }`}
                  />
                  <input
                    type="text"
                    name="tax_payer_id"
                    placeholder={t.popup_tax_payer_id}
                    {...register("tax_payer_id")}
                    className={`${styles.input100} ${
                      errors.tax_payer_id ? "is-invalid" : ""
                    }`}
                  />
                </>
              )}
            </div>
            <Button
              textBtn={
                <Price
                  price={discountTotalPrice ? discountTotalPrice : totalPrice}
                  checkout={true}
                />
              }
              customClass="largeBtn popupBtn"
              link="javascript:void(0);"
              submit={true}
              state={state}
            />
          </form>

          <div className="bepaid-checkout-container"></div>
        </div>
      </div>
    </>
  );
};

export default Checkout;
