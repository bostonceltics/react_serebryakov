import React from "react";
import { IoMdArrowDropdown } from "react-icons/io";
import styles from "./HeaderSelect.module.scss";

import { useStateContext } from "../../context/StateContext";

const LangSelect = () => {
  const { t, changeLanguage, siteLang } = useStateContext();

  return (
    <div className={styles.item}>
      <select value={siteLang} onChange={(e) => changeLanguage(e.target.value)}>
        <option value="ru">RU</option>
        <option value="en">EN</option>
      </select>{" "}
      <IoMdArrowDropdown size={25} />
    </div>
  );
};

export default LangSelect;
