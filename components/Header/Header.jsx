import React, { useState } from "react";
import Logo from "./Logo";
import LangSelect from "./LangSelect";
import CurencySelect from "./CurencySelect";
import Navbar from "./Navbar";
import { RxHamburgerMenu } from "react-icons/rx";
import { AiOutlineMinus } from "react-icons/ai";
import Link from "next/link";

import styles from "./Header.module.scss";
import headerMenu from "../../data/menu_header.json";

import { useStateContext } from "../../context/StateContext";

const Header = () => {
  const { useGetAPIData, isMobile, locale } = useStateContext();
  // let QUERY = encodeURIComponent('*[_type == "navigation"]');
  // let PROJECT_URL = `https://${process.env.NEXT_PUBLIC_PROJECT_ID}.api.sanity.io/v2021-10-21/data/query/${process.env.NEXT_PUBLIC_PROJECT_DATASET}?query=${QUERY}`;
  // let data = useGetAPIData(PROJECT_URL);
  // let headerMenu = [];
  // if (data) {
  //   headerMenu = data.result[0].header_menu;
  // }

  const [isShown, setIsShown] = useState(false);

  const showHide = (event) => {
    setIsShown((current) => !current);
  };

  return (
    <>
      {!isShown ? (
        <header className={styles.container}>
          {isMobile ? (
            <RxHamburgerMenu size={22} onClick={showHide} />
          ) : (
            <Navbar data={headerMenu} className="link uppercase" />
          )}

          <Logo />
          <div className={styles.wrap}>
            <CurencySelect />
            <LangSelect />
          </div>
        </header>
      ) : (
        <>
          <div className={styles.layout} onClick={showHide}></div>
          <div className={styles.mobMenu}>
            <div className={styles.mobMenu_top}>
              <Logo customClass="logo-left" />
              <AiOutlineMinus size={26} onClick={showHide} />
            </div>
            <ul>
              {headerMenu?.map((item, index) => (
                <li key={index}>
                  <Link
                    href={`/${item.slug ? item.slug : "/"}`}
                    className="link"
                    onClick={showHide}
                  >
                    {item[`text_${locale}`]}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
    </>
  );
};

export default Header;
