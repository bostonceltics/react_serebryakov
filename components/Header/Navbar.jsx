import React from "react";
import Link from "next/link";
import styles from "./Navbar.module.scss";

import { useStateContext } from "../../context/StateContext";

const Navbar = ({ data, className }) => {
  const { locale } = useStateContext();
  return (
    <ul className={styles.list}>
      {data?.map((item, index) => (
        <li key={index}>
          <Link href={`/${item.slug ? item.slug : "/"}`} className={className}>
            {item[`text_${locale}`]}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Navbar;
