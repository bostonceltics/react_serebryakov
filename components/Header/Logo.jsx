import React from "react";
import styles from "./Logo.module.scss";
import Link from "next/link";

const Logo = ({ customClass }) => {
  return (
    <Link href="/" className={`${styles.logo} ${customClass}`}>
      Serebryakov TF
    </Link>
  );
};

export default Logo;
