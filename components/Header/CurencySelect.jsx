import { useState, useEffect } from "react";
import { client } from "../../lib/client";
import { IoMdArrowDropdown } from "react-icons/io";
import Link from "next/link";
import Price from "../Elements/Price";
import styles from "./HeaderSelect.module.scss";
import { useStateContext } from "../../context/StateContext";

const CurencySelect = () => {
  const { totalPrice, changeCurrency, siteCurrency, discountTotalPrice } =
    useStateContext();

  const [data, setData] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const result = await client.fetch('*[_type == "currency"]');
      setData(result);
    };

    fetchData();
  }, []);

  return (
    <div className={styles.item}>
      {totalPrice !== 0 && (
        <Link href="/cart" className="link-line">
          <Price
            price={discountTotalPrice ? discountTotalPrice : totalPrice}
            symbol="false"
          />
        </Link>
      )}
      <select
        value={siteCurrency}
        onChange={(e) => changeCurrency(e.target.value)}
      >
        {data && data[0].byn === true && <option value="BYN">BYN</option>}
        {data && data[0].rub === true && <option value="RUB">RUB</option>}

        <option value="USD">USD</option>
      </select>{" "}
      <IoMdArrowDropdown size={25} />
    </div>
  );
};

export default CurencySelect;
