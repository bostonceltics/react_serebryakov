import React, { useEffect } from "react";
import Head from "next/head";

import styles from "./Layout.module.scss";
import { useStateContext } from "../../context/StateContext";

const Layout = ({ children }) => {
  const { isMobile } = useStateContext();

  return (
    <div className={styles.container}>
      <Head>
        <title>Serebryakov</title>
        <meta
          name="viewport"
          content={`width=${
            isMobile ? "768" : "device-width"
          }, maximum-scale=1, initial-scale=1`}
        ></meta>
        <script
          defer
          type="text/javascript"
          src="https://js.bepaid.by/widget/be_gateway.js"
        ></script>
      </Head>
      {children}
    </div>
  );
};

export default Layout;
