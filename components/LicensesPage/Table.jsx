import React from "react";
import Link from "next/link";
import styles from "./Table.module.scss";
import { useStateContext } from "../../context/StateContext";

const Table = ({ type, data }) => {
  const { getPreviewLink, locale, t } = useStateContext();
  return (
    <>
      {type === "faq" ? (
        <div className={styles.table}>
          {data?.map((item, index) => {
            return (
              <div key={index} className={`${styles.row} ${styles.faqRow}`}>
                <div className={styles.col}>
                  <p>{item[`question_${locale}`]}</p>
                </div>
                <div className={styles.col}>
                  <p>{item[`answer_${locale}`]}</p>
                </div>
              </div>
            );
          })}
        </div>
      ) : (
        <div className={styles.table}>
          {data?.map((item, index) => {
            return (
              <div key={index} className={`${styles.row} ${styles.licenseRow}`}>
                <div className={styles.col}>
                  <p className="uppercase">{item[`title_${locale}`]}</p>
                  {item[`file_${locale}`] ? (
                    <Link
                      href={getPreviewLink(item[`file_${locale}`].asset._ref)}
                      className="link-line"
                      target="_blank"
                    >
                      {t.full_license}
                    </Link>
                  ) : (
                    <Link
                      href="mailto:fonts@serebryakov.com"
                      className="link-line"
                      target="_blank"
                    >
                      {t.request_a_quote}
                    </Link>
                  )}
                </div>
                <div className={styles.col}>
                  <p>{item[`who_${locale}`]}</p>
                </div>
                <div className={styles.col}>
                  <p>{item[`desc_${locale}`]}</p>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default Table;
