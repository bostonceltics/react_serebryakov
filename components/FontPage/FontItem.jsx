import React, { useState } from "react";
import Button from "../Elements/Button";
import Price from "../Elements/Price";
import Preview from "./Preview";
import SizeRange from "./SizeRange";
import FamilyPreview from "./FamilyPreview";
import Variation from "./Variation";
import Style from "../Elements/Style";
import styles from "./FontItem.module.scss";
import { useStateContext } from "../../context/StateContext";
import tarifs from "../../data/tarifs.json";
import licenses from "../../data/licenses.json";

const FontItem = ({
  family,
  title,
  amount,
  fonts,
  name,
  family_prices,
  prices,
  word,
  lowercase,
  style,
  styleLink,
  index,
  licensesSanity,
}) => {
  const { getMinPriceStyle, getPreviewLink, t } = useStateContext();
  const [rangeValue, setRangeValue] = useState(216);

  const [isShown, setIsShown] = useState(false);

  const showHide = (event) => {
    setIsShown((current) => !current);
  };
  let preview = "";
  if (styleLink) {
    preview = getPreviewLink(styleLink);
  }

  return (
    <>
      {preview && <Style slug={`${style}`} preview={preview} />}
      <div className={`${styles.item} font-item`}>
        <div className={styles.top}>
          <p>
            {family === true && amount > 1
              ? `${title} Family, ${amount} ${t.styles}`
              : `${name}`}
          </p>
          {family !== true && (
            <SizeRange
              rangeValue={rangeValue}
              setRangeValue={setRangeValue}
              active={isShown ? true : false}
            />
          )}

          <Button
            textBtn={
              <Price
                price={getMinPriceStyle(
                  family === true && amount > 1 ? family_prices : prices
                )}
                min={true}
              />
            }
            link="javascript:void(0);"
            showHide={showHide}
            isShown={isShown}
          />
        </div>

        {family === true && amount > 1 ? (
          <FamilyPreview key={fonts} fonts={fonts} />
        ) : (
          <Preview
            word={word}
            lowercase={lowercase}
            style={{
              fontFamily: `${style}, sans-serif`,
              fontSize: `${rangeValue}px`,
            }}
          />
        )}

        {isShown && (
          <div style={{ marginBottom: "50px" }}>
            {licenses?.map((license, variationIndex) => (
              <Variation
                key={license.name}
                license={license}
                tarifs={tarifs}
                prices={family === true && amount > 1 ? family_prices : prices}
                family={family === true && amount > 1 ? true : false}
                style={style ? style : "family"}
                stylePosition={index}
                variationPosition={variationIndex}
                hintText={licensesSanity?.find(
                  (item) => item.slug === license.name
                )}
                // slug={font.slug.current}
              />
            ))}
          </div>
        )}
      </div>
    </>
  );
};

export default FontItem;
