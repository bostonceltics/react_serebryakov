import React from "react";
import styles from "./FontItem.module.scss";

const Preview = ({ word, lowercase, style }) => {
  let previewSymbols = "";

  if (lowercase) {
    previewSymbols = "AaBbCcDdEeFfGgHhIiGgKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
  } else {
    previewSymbols = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
  }

  return (
    <div
      className={styles.preview}
      spellCheck="false"
      contentEditable=""
      style={style}
    >
      {word ? word : previewSymbols}
    </div>
  );
};

export default Preview;
