import React from "react";
import styles from "./FontItem.module.scss";

const FamilyPreview = ({ fonts }) => {
  return (
    <div className={styles.styles}>
      {fonts?.map((font, index) => (
        <div
          key={index}
          className={`${styles.stylesItem} capitalize`}
          style={{ fontFamily: `${font.style}, sans-serif` }}
        >
          {font.style}
        </div>
      ))}
    </div>
  );
};

export default FamilyPreview;
