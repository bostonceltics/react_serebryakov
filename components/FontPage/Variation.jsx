import React, { useState } from "react";
import { IoMdArrowDropdown } from "react-icons/io";
import Button from "../Elements/Button";
import Price from "../Elements/Price";
import Hint from "../Elements/Hint";
import styles from "./Variation.module.scss";
import { useStateContext } from "../../context/StateContext";

const Variation = ({
  license,
  tarifs,
  prices,
  family,
  style,
  stylePosition,
  variationPosition,
  hintText,
}) => {
  const { onAdd, getPreviewLink, fontSlug, checkCartItems, locale, t } =
    useStateContext();

  const first = tarifs.find((tarif) => tarif.fieldset === license.name);

  const [variationPrice, setVariationPrice] = useState(
    family === true
      ? prices[`family_${first.fieldset}${first.name}`]
      : prices[`${first.fieldset}${first.name}`]
  );
  const [variation, setVariation] = useState(
    family === true
      ? `family_${first.fieldset}${first.name}`
      : `${first.fieldset}${first.name}`
  );

  const changeVariationPrice = (prices, name) => {
    const newPrice = prices[`${name}`];
    const newVariation = `${name}`;
    setVariationPrice(newPrice);
    setVariation(newVariation);
  };

  let product = {
    variation: variation,
    style: style,
    family: family,
    name: fontSlug,
    price: variationPrice,
    stylePosition: stylePosition,
    variationPosition: variationPosition,
  };

  let checkCartItemsResult;
  if (product) {
    checkCartItemsResult = checkCartItems(product);
  }

  return (
    <div className={styles.item}>
      <div className={styles.col}>
        <p className="hint-wrap">
          {locale === "en" ? license.title : license.title_ru}
          <Hint
            text={hintText && hintText[`hint_${locale}`]}
            link={
              hintText && getPreviewLink(hintText[`file_${locale}`].asset._ref)
            }
          />
        </p>
      </div>
      <div className={styles.col}>
        <select
          id="select"
          onChange={(e) => changeVariationPrice(prices, e.target.value)}
        >
          {tarifs?.map(
            (tarif, index) =>
              tarif.fieldset === license.name && (
                <option
                  key={index}
                  value={
                    family === true
                      ? `family_${tarif.fieldset}${tarif.name}`
                      : `${tarif.fieldset}${tarif.name}`
                  }
                >
                  {
                    <span
                      dangerouslySetInnerHTML={{
                        __html: locale === "en" ? tarif.title : tarif.title_ru,
                      }}
                    />
                  }
                </option>
              )
          )}
        </select>
        <IoMdArrowDropdown size={25} />
      </div>
      <div className={styles.col}>
        <div>
          <p>
            <Price price={variationPrice} />
          </p>
        </div>
        <div>
          <Button
            textBtn={
              checkCartItemsResult
                ? checkCartItemsResult.text
                : t.add_to_cart_btn
            }
            customClass={`smallBtn whiteBtn ${
              checkCartItemsResult ? checkCartItemsResult.class : ``
            }`}
            link="javascript:void(0);"
            onAdd={() => onAdd(product)}
          />
        </div>
      </div>
    </div>
  );
};

export default Variation;
