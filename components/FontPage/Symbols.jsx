import React, { useState, useEffect } from "react";
import { IoMdArrowDropdown } from "react-icons/io";
import Style from "../Elements/Style";
import RadioButton from "../Elements/RadioButton";
import styles from "./Symbols.module.scss";
import { useStateContext } from "../../context/StateContext";

const Symbols = ({ previewLink, fontStyle, italic }) => {
  const { changeRadioValue } = useStateContext();
  const [checkValue, setCheckValue] = useState("roman");

  useEffect(() => {
    if (italic) {
      changeRadioValue(setCheckValue, checkValue);
    }
  }, []);

  const basicLatin = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  const basicCyrillic =
    "АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзиклмнопрстуфхцчшщъыьэюя";
  const numbers = "0123456789";
  const punctuations = `.,:;…!¡?¿·•*#/(){}[]-­–—_‚„“”‘’«»‹›"'`;
  const symbols = "$€₴£¥+−×÷=><±≈~^%↑→↓←@&¶§©®™°†№";

  const [symbolsArray, setSymbolsArray] = useState(basicLatin);

  const changeSymbolsTable = (value) => {
    if (value === "basicLatin") {
      setSymbolsArray(basicLatin);
    }
    if (value === "basicCyrillic") {
      setSymbolsArray(basicCyrillic);
    }
    if (value === "numbers") {
      setSymbolsArray(numbers);
    }
    if (value === "punctuations") {
      setSymbolsArray(punctuations);
    }
    if (value === "symbols") {
      setSymbolsArray(symbols);
    }
  };
  return (
    <>
      {previewLink && <Style slug={`${fontStyle}`} preview={previewLink} />}
      <div className={styles.container}>
        <div className={styles.top}>
          <div>
            <select
              id="select"
              onChange={(e) => changeSymbolsTable(e.target.value)}
            >
              <option value="basicLatin">Basic Latin</option>
              <option value="basicCyrillic">Basic Cyrilic</option>
              <option value="numbers">Numbers</option>
              <option value="punctuations">Punctuations</option>
              <option value="symbols">Symbols</option>
            </select>
            <IoMdArrowDropdown size={25} />
          </div>
          <div>
            {italic && (
              <div
                onChange={(e) =>
                  changeRadioValue(setCheckValue, e.target.value)
                }
              >
                <RadioButton id="roman" name="symbolStyle" text="Roman" />
                <RadioButton id="italic" name="symbolStyle" text="Italic" />
              </div>
            )}
          </div>
        </div>
        <div
          className={styles.grid}
          style={{
            fontFamily: `${fontStyle}, sans-serif`,
            fontStyle: `${checkValue == "italic" ? "italic" : "inherit"}`,
          }}
        >
          {symbolsArray.split("")?.map((item, index) => (
            <div key={index} className={styles.cell}>
              {item}
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Symbols;
