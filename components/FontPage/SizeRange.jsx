import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import styles from "./FontItem.module.scss";

// const CustomSlider = withStyles({
//   track: {
//     backgroundColor: "#FF0000",
//   },
// })(Slider);

const CustomSlider = withStyles({
  track: {
    backgroundColor: "#000",
    height: "1px",
    opacity: "1",
  },
  rail: {
    backgroundColor: "#000",
    height: "1px",
    opacity: "1",
  },
  thumb: {
    width: "19px",
    height: "19px",
    top: "8px",
    borderRadius: "50%",
    backgroundColor: "#000",
    border: "1px solid #000",
    "&:hover": {
      boxShadow: `none`,
    },
    "&.Mui-focusVisible": {
      boxShadow: `none`,
    },
    active: {
      boxShadow: `none`,
    },
  },
})(Slider);

const SizeRange = ({ setRangeValue, rangeValue, active }) => {
  const handleChange = (event, newValue) => {
    setRangeValue(newValue);
  };

  // const classes = useStyles();

  return (
    <>
      <div
        className={`${styles.rangeWrap} ${
          active === true ? "range-slider-active" : ""
        } range-slider`}
      >
        <CustomSlider
          value={rangeValue}
          onChange={handleChange}
          aria-label="Volume"
          min={40}
          max={432}
        />
      </div>
    </>
  );
};

export default SizeRange;
