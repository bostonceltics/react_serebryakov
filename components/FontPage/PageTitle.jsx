import React from "react";
import styles from "./PageTitle.module.scss";

const PageTitle = ({ title, className, show }) => {
  return (
    <h1
      className={`${styles.text} ${className ? className : ""}`}
      onClick={show && show}
    >
      {title}
    </h1>
  );
};

export default PageTitle;
