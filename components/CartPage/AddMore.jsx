import React from "react";
import styles from "./AddMore.module.scss";

const AddMore = () => {
  return (
    <div className={styles.item}>
      <p>+ Add more</p>
      <select name="" className="invisible">
        <option value="0">Choose a license type</option>
        <option value="1">Базовая</option>
        <option value="2">Веб</option>
      </select>
    </div>
  );
};

export default AddMore;
