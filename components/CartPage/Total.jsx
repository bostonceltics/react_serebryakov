import React from "react";
import Price from "../Elements/Price";
import styles from "./Total.module.scss";
import { useStateContext } from "../../context/StateContext";

const Total = ({ coupons }) => {
  const {
    totalPrice,
    discount,
    checkCoupons,
    couponText,
    setCouponsArr,
    discountTotalPrice,
    t,
  } = useStateContext();
  setCouponsArr(coupons);
  return (
    <div className={styles.item}>
      <p className="uppercase">{t.total_title}</p>
      <div className={styles.grid}>
        <div className={styles.row}>
          <div className={styles.col}>
            <p>{t.discount}</p>
          </div>
          <div className={styles.col}>
            <input
              id="promoCodeInput"
              type="text"
              placeholder={t.input_promo}
              value={couponText}
              onChange={(e) => checkCoupons(e.target.value, coupons)}
              className={`${styles.col} ${styles.promo}`}
            />
          </div>
          <div className={styles.col}>
            <p>
              {discount > 0 && "-"}
              <Price price={discount} />
            </p>
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.col}>
            <p>{t.total}</p>
          </div>
          <div className={styles.col}>
            <p>
              <Price
                price={discountTotalPrice ? discountTotalPrice : totalPrice}
              />
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Total;
