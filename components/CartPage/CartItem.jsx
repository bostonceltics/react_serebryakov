import React from "react";
import Link from "next/link";
import VariationCart from "./VariationCart";
import AddMore from "./AddMore";
import styles from "./CartItem.module.scss";
import licensesData from "../../data/licenses.json";
import tarifsData from "../../data/tarifs.json";
import { useStateContext } from "../../context/StateContext";

const CartItem = ({ index, font, styleName, licenses, licensesSanity }) => {
  const { getAmountStyles, t, locale } = useStateContext();

  const amount = getAmountStyles(font.fonts);

  return (
    <div className={`${styles.item} cart-item`}>
      <p className={styles.top}>
        <span className={styles.number}>{index}.</span>
        <span className="font_name">
          <Link href={`/font/${font.slug.current}`} className="link-line">
            {font.title}
          </Link>{" "}
          {styleName.charAt(0).toUpperCase() + styleName.slice(1)}
        </span>
        ,{" "}
        {styleName === "family"
          ? (amount === 2 || amount === 3 || amount === 4) && locale === "ru"
            ? `${amount} начертания`
            : `${amount} ${t.styles}`
          : `1 ${t.style}`}
      </p>
      {licenses?.map((item, index) => {
        const arrFromVariation = item.variation.split("_");
        let licenseName;
        if (item.family == false) {
          licenseName = arrFromVariation[0];
        } else {
          licenseName = arrFromVariation[1];
        }

        const info = licensesData.find(
          (license) => license.name == licenseName
        );
        return (
          <VariationCart
            key={index}
            info={info}
            tarifs={tarifsData}
            product={item}
            font={font}
            fonts={font.fonts}
            hintText={licensesSanity?.find(
              (licenseSanity) => licenseSanity.slug === info.name
            )}
          />
        );
      })}

      <AddMore />
    </div>
  );
};

export default CartItem;
