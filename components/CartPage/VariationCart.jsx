import React from "react";
import * as Yup from "yup";
import Link from "next/link";
import Price from "../Elements/Price";
import Hint from "../Elements/Hint";
import { IoMdArrowDropdown } from "react-icons/io";
import styles from "../FontPage/Variation.module.scss";
import { useStateContext } from "../../context/StateContext";

const VariationCart = ({ info, tarifs, product, font, fonts, hintText }) => {
  const {
    onRemove,
    onUpdate,
    onUpdateInput,
    getPreviewLink,
    register,
    errors,
    shape,
    trigger,
    locale,
    t,
  } = useStateContext();

  const font_prices = fonts.find((item) => item.style == product.style);
  let prices;
  if (font_prices) {
    prices = font_prices.prices;
  }

  const switchParams = () => {
    switch (info.name) {
      case "web":
        shape[`${product.variation}`] = Yup.string()
          .matches(
            /((https?):\/\/)?(www.)?[a-zа-я0-9]+(\.[a-zа-я]{2,}){1,3}(#?\/?[a-zа-я0-9#]+)*\/?(\?[a-zа-я0-9-_]+=[a-zа-я0-9-%]+&?)?$/,
            "Enter correct url!"
          )
          .required("Please enter website");
        return (
          <input
            type="text"
            placeholder={t.input_web}
            {...register(product.variation)}
            className={`${styles.col} ${
              errors[`${product.variation}`] ? "is-invalid-full" : ""
            } input_value`}
            autocomplete="off"
            value={product.inputField ? product.inputField : ""}
            onChange={(e) => {
              onUpdateInput(product, e.target.value);
            }}
            // onClick={async () => {
            //   const result = await trigger(product.variation);
            // }}
          />
        );
      case "app":
        shape[`${product.variation}`] = Yup.string().required(
          "Product name is required"
        );
        return (
          <input
            type="text"
            placeholder={t.input_app}
            {...register(product.variation)}
            className={`${styles.col} ${
              errors[`${product.variation}`] ? "is-invalid-full" : ""
            } input_value`}
            autocomplete="off"
            value={product.inputField ? product.inputField : ""}
            onChange={(e) => {
              async () => {
                const result = await trigger(product.variation);
              };
              onUpdateInput(product, e.target.value);
            }}
          />
        );
      case "social":
        shape[`${product.variation}`] = Yup.string().required(
          "Account link is required"
        );
        return (
          <input
            type="text"
            placeholder={t.input_social}
            {...register(product.variation)}
            className={`${styles.col} ${
              errors[`${product.variation}`] ? "is-invalid-full" : ""
            } input_value`}
            autocomplete="off"
            value={product.inputField ? product.inputField : ""}
            onChange={(e) => {
              async () => {
                const result = await trigger(product.variation);
              };
              onUpdateInput(product, e.target.value);
            }}
          />
        );

      default:
        return <div></div>;
    }
  };

  return (
    <div className={`${styles.item} variation-cart`}>
      <div className={styles.col}>
        <p className="hint-wrap">
          <div className="font_license">
            {locale === "en" ? info.title : info.title_ru}
          </div>
          <Hint
            text={hintText && hintText[`hint_${locale}`]}
            link={
              hintText && getPreviewLink(hintText[`file_${locale}`].asset._ref)
            }
          />
        </p>
      </div>
      <div className={styles.col}>
        <select
          id="select"
          value={product.variation}
          onChange={(e) => {
            onUpdate(
              product,
              e.target.value,
              product.family === true
                ? font.family_prices[`${e.target.value}`]
                : prices[`${e.target.value}`]
            );
          }}
        >
          {tarifs?.map(
            (item, index) =>
              item.fieldset === info.name && (
                <option
                  key={index}
                  value={
                    product.family === true
                      ? `family_${item.fieldset}${item.name}`
                      : `${item.fieldset}${item.name}`
                  }
                >
                  {
                    <span
                      dangerouslySetInnerHTML={{
                        __html: locale === "en" ? item.title : item.title_ru,
                      }}
                    />
                  }
                </option>
              )
          )}
        </select>
        <IoMdArrowDropdown size={25} />
      </div>

      <div
        className={`${styles.col} ${
          (info.name == "web" || info.name == "app" || info.name == "social") &&
          styles.itemInputCol
        }`}
      >
        {switchParams()}
        <div>
          <p className={`${styles.price} font_price`}>
            <Price price={product.price} />
          </p>
          <Link
            href="javascript:void(0);"
            className="link"
            onClick={() => onRemove(product)}
          >
            {t.remove_btn}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default VariationCart;
